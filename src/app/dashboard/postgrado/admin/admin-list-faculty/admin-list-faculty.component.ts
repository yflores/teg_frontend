import { Component, OnInit } from '@angular/core';
import { FacultadModel } from '../../../common-dashboard/models/facultad.model';
import { FacultadService } from '../../../common-dashboard/services/facultad.service';
import { MatDialog } from '@angular/material';
import { ModalCreateFacultadComponent } from './modal-create-facultad/modal-create-facultad.component';

@Component({
  selector: 'admin-list-faculty',
  templateUrl: './admin-list-faculty.component.html',
  styleUrls: ['./admin-list-faculty.component.css']
})
export class AdminListFacultyComponent implements OnInit {

  facultades : FacultadModel[] = [];
  facultad: FacultadModel;
  spinner = true;

  constructor(
    private facultadService : FacultadService,
    public dialog: MatDialog
    ) { }

  ngOnInit() {
    this.facultad = new FacultadModel(0, "", "");
    this.facultadService.getFacultades().subscribe(facultades => {
      this.facultades = facultades;
      console.log("Salida ->. ", facultades);
      this.spinner = false;
    });
  }

  openModalNewFacultad() {
    const dialogRef = this.dialog.open(ModalCreateFacultadComponent, {
      height: '350px',
      width: '500px',
      data: {
        title: "Nueva Facultad",
        facultad: this.facultad
      }
    });
    dialogRef.beforeClose().subscribe(facultad => {
     this.ngOnInit();
    });
  }

  openModalEditFacultad(facultad: FacultadModel) {
    const dialogRef = this.dialog.open(ModalCreateFacultadComponent, {
      height: '350px',
      width: '500px',
      data: {
        title: "Editar Facultad",
        facultad: facultad
      }
    });
    dialogRef.beforeClose().subscribe(idCombo => {
     this.ngOnInit();
    });
  }

  deleteFacultad(facultad: FacultadModel) {
    this.facultadService.postdeleteFacultad(facultad).subscribe(facultad => {
      console.log("Facultad Eliminada...");
      this.ngOnInit();
    });
  }
}
