import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminListFacultyComponent } from './admin-list-faculty/admin-list-faculty.component';
import { DetailFacultyProgramComponent } from './detail-faculty-program/detail-faculty-program.component';
import { ViewUserComponent } from './admin-list-faculty/view-user/view-user.component';

const routes: Routes = [
    { 
        path: 'main',
        component: AdminListFacultyComponent 
    },
    {
        path: ':/facultad',
        children: [
            {
                path: ':id_facultad',
                component: DetailFacultyProgramComponent
            }
        ]
    },
    {
        path: ':/users',
        children: [
            {
                path: ':id_facultad',
                component: ViewUserComponent
            }
        ]
    }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
