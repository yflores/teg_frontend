import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import {

 } from '@angular/cdk/table';

import 'rxjs/add/operator/switchMap';
import { ItemsComboModel, ComboModel } from '../../../../../common-dashboard/models/evaluation.model';
import { ComboService } from '../../../../../common-dashboard/services/combo.service';
import { ItemsComboService } from '../../../../../common-dashboard/services/itemsCombo.service';

@Component({
  selector: 'app-crear-combo',
  templateUrl: './crear-combo.component.html',
  styleUrls: ['./crear-combo.component.css']
})
export class CrearComboComponent implements OnInit {
  combo: ComboModel;
  itemsCombo: ItemsComboModel[] = [];
  item: ItemsComboModel;
  msj: string;
  comboSaved: ComboModel;
  itemsComboSaved: ItemsComboModel;

  constructor(
    public dialogRef: MatDialogRef<CrearComboComponent>,
    private comboService: ComboService,
    private itemsComboService: ItemsComboService,
  ) { }

  ngOnInit() {
    this.combo = new ComboModel(undefined, undefined, undefined);
    this.initItemCombo();
  }

  initItemCombo() {
    this.item = new ItemsComboModel(undefined, undefined, undefined);
  }

  onSaveItemCombo(item) {
    this.itemsCombo.push(item);
    console.log("Items de Combo -->: ", this.itemsCombo);
    this.initItemCombo();
  }

  onSaveCombo(combo, item) {
    this.itemsCombo.push(item);
    this.comboService.postCombo(combo)
        .subscribe(element => {
          this.comboService.getComboByName(element.cmb_titulo)
              .subscribe(combosaved => {
                this.comboSaved = combosaved;
                console.log("Combo Guardado ->: ", combo);
                console.log("Items de Combo guardado -->: ", this.itemsCombo);
                this.itemsCombo.forEach(element =>{
                  console.log("validation Final ---->: ", element);
                  this.itemsComboService.postItemsCombo(element, this.comboSaved.cmb_id)
                      .subscribe(itemsComboSaved => {
                        this.itemsComboSaved = itemsComboSaved;
                        console.log("Item de Combo Guardado ->: ", this.itemsComboSaved);
                        this.dialogRef.close();
                      });
                  });
              });
          });
  }

  onChange(value) {
    console.log("value -->: ", value);
  }

  goBack() {
    this.dialogRef.close();
  }
}
