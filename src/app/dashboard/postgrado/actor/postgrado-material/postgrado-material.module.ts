import { NgModule } from '@angular/core';
import { MatIconModule, MatDialogModule, MatCardModule, MatExpansionModule, MatTooltipModule, MatSelectModule } from '@angular/material';

@NgModule({
    imports: [
        MatIconModule,
        MatCardModule,
        MatDialogModule,
        MatExpansionModule,
        MatTooltipModule,
        MatSelectModule
    ],
    exports: [
        MatIconModule,
        MatCardModule,
        MatDialogModule,
        MatExpansionModule,
        MatTooltipModule,
        MatSelectModule
    ]
})
export class PostgradoMaterialModule {}
