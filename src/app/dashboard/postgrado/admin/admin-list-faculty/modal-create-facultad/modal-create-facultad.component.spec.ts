import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCreateFacultadComponent } from './modal-create-facultad.component';

describe('ModalCreateFacultadComponent', () => {
  let component: ModalCreateFacultadComponent;
  let fixture: ComponentFixture<ModalCreateFacultadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCreateFacultadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCreateFacultadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
