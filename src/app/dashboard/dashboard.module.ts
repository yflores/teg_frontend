import { DashboardMaterialModule } from './common-dashboard/dashboard-material/dashboard-material.module';
import { CommonDashboardModule } from './common-dashboard/common-dashboard.module';
import { PostgradoModule } from './postgrado/postgrado.module';
import { NgModule } from '@angular/core';

import { routing } from './dashboard.routing';

import { DashboardComponent } from './dashboard.component';

import { ActorService } from './common-dashboard/services/actor.service';
import { LoginService } from './common-dashboard/services/login.service';
import { InstrumentService } from './common-dashboard/services/instrument.service';

@NgModule({
    imports: [
        CommonDashboardModule,
        DashboardMaterialModule,
        PostgradoModule,
        routing,
    ],
    declarations: [
        DashboardComponent
    ],
    providers: [
        ActorService,
        LoginService,
        InstrumentService,
    ],
    entryComponents: []
})
export class DashboardModule {}
