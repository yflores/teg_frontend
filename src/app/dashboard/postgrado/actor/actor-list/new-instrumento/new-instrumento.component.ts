
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActorService } from '../../../../common-dashboard/services/actor.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InstrumentoModel } from '../../../../common-dashboard/models/instrumento.models';
import { PreguntaModel, ItemModel, EvaluacionModel, ComboModel, ItemSelectionMultipleModel } from '../../../../common-dashboard/models/evaluation.model';
import { InstrumentService } from '../../../../common-dashboard/services/instrument.service';
import { ComboSubPreguntaComponent } from '../../actor-detail/combo-sub-pregunta/combo-sub-pregunta.component';
import { PreguntaService } from '../../../../common-dashboard/services/pregunta.service';
import { EvaluacionService } from '../../../../common-dashboard/services/evaluacion.service';
import { ComboService } from '../../../../common-dashboard/services/combo.service';
import { ModalEditPreguntaComponent } from './modal-edit-pregunta/modal-edit-pregunta.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Information } from '../../../../../information';
import { ComboPreguntaSeleccionMultipleComponent } from './../../actor-detail/combo-pregunta-seleccion-multiple/combo-pregunta-seleccion-multiple.component';

@Component({
  selector: 'app-new-instrumento',
  templateUrl: './new-instrumento.component.html',
  styleUrls: ['./new-instrumento.component.css']
})
export class NewInstrumentoComponent implements OnInit {
  dialogRef: any;
  instrumento: InstrumentoModel;
  instrumentoSub: InstrumentoModel;
  pregunta: PreguntaModel;
  item: ItemModel;
  items: ItemModel[];
  evaluacion: PreguntaModel[];
  evaluation: EvaluacionModel;
  public editor;
  public editorOptions = {
    placeholder: 'Ingrese una descripción a la Evaluación...'
  };
  combo: ComboModel;
  messageObservation = 'Descripción corta.';
  form: FormGroup;
  itemSeleccionMultiple: ItemSelectionMultipleModel[] = [];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router,
    private intrumentService: InstrumentService,
    private evaluacionService: EvaluacionService,
    private preguntaService: PreguntaService,
    private comboService: ComboService,
    fb: FormBuilder
  ) {
    this.form = fb.group({
      profesor: ['', Validators.required],
      participante: ['', Validators.required],
      empleado: ['', Validators.required],
      egresado: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.instrumento = new InstrumentoModel(undefined, undefined, undefined, undefined, undefined, undefined, '', 0, 0, 0, 0, undefined, 0);
    console.log(this.instrumento);
    this.evaluacion = new Array();
    this.items = new Array();
    this.initialization();
  }

  onChange(searchValue) {
    console.log(searchValue);
  }

  goBack() {
    this.dialogRef.close();
  }

  guardarEvaluacion(instrumento) {
    instrumento.fac_id = Information.getCookieByName('facultyId');
    console.log('Instrumento -->: ', instrumento);
    console.log('Evaluacion -->: ', this.evaluacion);
    console.log('items -->: ', this.items);
    this.intrumentService.postInstrument(instrumento)
        .subscribe(element => {
          console.log('');
        });

    this.evaluation = new EvaluacionModel(undefined, instrumento.int_codigo, instrumento.int_programa,
                          undefined, undefined, undefined, undefined, undefined);
    this.evaluacionService.postInstrument(this.evaluation)
        .subscribe(element => {
          console.log('Evaluación Guardada con Éxito...');
          this.evaluacionService.getEvaluacionByCodeInstrument(instrumento.int_codigo)
                        .subscribe(element => {
                          this.evaluation = element;
                          this.evaluacion.forEach(element => {
                            element.eval_id = this.evaluation.eval_id;
                            this.preguntaService.postPreguntas(element)
                                .subscribe(pregunta => {
                                  console.log('Pregunta Guardada con Éxito...');
                                  this.router.navigate(['/dashboard/postgrado/actor', Information.getCookieByName('role')]);
                                });
                            });
                        });
        });
  }

  initialization(): void {
    this.pregunta = new PreguntaModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined);

    this.item = new ItemModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined,
      undefined, undefined, undefined, undefined, undefined, undefined, new Array(), undefined, undefined);
  }

  onChangePregunta(searchValue) {
    if (this.pregunta.sub.toString() === 'Si') {
      this.pregunta.pre_sub = 1;
    } else {
      this.pregunta.pre_sub = 0;
    }
  }

  onChangeTypeSubPregunta(searchValue) {
    console.log("Cuando le doy al combo --->: ", this.item);
    if (this.item.seleccion !== undefined) {
      if (this.item.seleccion.toString() === 'Si') {
        this.item.itm_seleccion = 1;
      } else {
        this.item.itm_seleccion = 0;
      }
    }
    if (this.item.combo !== undefined) {
      if (this.item.combo.toString() === 'Si') {
        this.item.itm_combo = 1;
        let idCombo: number;
        const dialogRef = this.dialog.open(ComboSubPreguntaComponent, { data: { idCombo: idCombo } });
        dialogRef.beforeClose().subscribe(idCombo => {
          this.item.num_combo = dialogRef.componentInstance.idCombo;
          console.log('this.item.num_combo -->', this.item.num_combo);
        });
      } else {
        this.item.itm_combo = 0;
      }
    }
    if (this.item.observacion !== undefined) {
      if (this.item.observacion.toString() === 'Si') {
        this.item.itm_observacion = 1;
        const dialogRef = this.dialog.open(ComboPreguntaSeleccionMultipleComponent, { data: { itemSeleccionMultiple : this.itemSeleccionMultiple } });
        dialogRef.beforeClose().subscribe(itemSeleccionMultiple => {
          console.log("valor de seleccion -->: ", dialogRef.componentInstance.itemSeleccionMultiple);
          this.item.itm_value_seleccion = dialogRef.componentInstance.itemSeleccionMultiple;
        });
      } else {
        this.item.itm_observacion = 0;
      }
    }
    console.log('pregunta.pre_sub -->: ', this.pregunta.pre_sub);
    console.log('item.itm_seleccion -->', this.item.itm_seleccion);
    console.log('item.itm_combo -->', this.item.itm_combo);
    console.log('item.itm_observacion -->', this.item.itm_observacion);
  }

  accept(): void {
    //this.navactor.back();
  }

  addNewPregunta(): void {
    console.log('Whats --->: ', this.items);
    this.pregunta.pre_sub_pregunta = this.items;
    this.evaluacion.push(this.pregunta);
    console.log('Evaluacion --->: ', this.evaluacion);
    this.initialization();
    this.items = new Array();

  }

  addComboItems(numberCombo) {
    this.item.num_combo = numberCombo;
  }

  addNewItem(): void {
    if (this.item.num_combo !== undefined) {
      this.comboService.getNameComboByid(this.item.num_combo).subscribe(combo => {
        this.item.name_combo = combo;
        console.log("Miraddd -->: ", combo);
        this.items.push(this.item);
        console.log("Item --->: ", this.items);
        this.initializationItems();
      });
    } else {
      console.log("mira el valor -->: ", this.item);
      this.items.push(this.item);
      console.log('Item --->: ', this.items);
      this.initializationItems();
    }
  }

  initializationItems(): void {
    this.item = new ItemModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined,
      undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
  }

  openModalCombo() {
    const dialogRef = this.dialog.open(ComboSubPreguntaComponent);
  }

  onEditorBlured(quill) {
    console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
    console.log('quill is ready! this is current quill instance object', quill);
  }

  onContentChanged({ quill, html, text }) {
    console.log('quill content is changed!', quill, html, text);
    this.instrumento.int_descripcion = html;
  }

  openModalEditSubPregunta(subpregunta) {
    const dialogRef = this.dialog.open(ModalEditPreguntaComponent, {
      height: '200px',
      width: '1005px',
      data: {
        subpregunta: subpregunta,
        title: 'Editar subpregunta',
        modalType: 1
      }
    });
    dialogRef.beforeClose().subscribe(idCombo => {
     console.log('Modal Cerrada.');
    });
  }

  deleteSubPregunta(element: ItemModel) {
    console.log('Elemento a eliminar ->: ', element);
    let i = 0;
    let j = 0;
    let encontrado = false;
    for (const pregunta of this.evaluacion) {
      for (const subpregunta of pregunta.pre_sub_pregunta) {
        if (subpregunta === element) {
          encontrado = true;
          break;
        } else {
          j ++;
        }
      }
      if (!encontrado) {
        i ++;
      } else {
        break;
      }
    }
    console.log('i ->: ', i, ' j ->: ', j);
    this.evaluacion[i].pre_sub_pregunta.splice(j, 1);
  }

  deletePregunta(pregunt : PreguntaModel) {
    console.log("Pregunta -->: ", pregunt);
    let i = 0;
    let encontrado = false;
    for (const pregunta of this.evaluacion) {
      if (pregunta === pregunt) {
        encontrado = true;
        break;
      } else {
        i ++;
      }
    }
    this.evaluacion.splice(i, 1);
  }

  openModalEditPregunta(pregunta: PreguntaModel) {
    const dialogRef = this.dialog.open(ModalEditPreguntaComponent, {
      height: '400px',
      width: '1005px',
      data: {
        pregunta: pregunta,
        title: 'Editar pregunta',
        modalType: 0
      }
    });
    dialogRef.beforeClose().subscribe(idCombo => {
     console.log('Modal Cerrada.');
    });
  }

  resetRadio(event: any, value: string) {
    const typePerson = 'int_' + value;
    let numberType = 1;
    if (this.form.get(value).value === event.target.value){
        this.form.get(value).setValue('false');
        numberType = 0;
    }
    this.instrumento.setypePerson(typePerson, numberType);
  }
}
