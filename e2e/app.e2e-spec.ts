import { MonitoringServicesFrontPage } from './app.po';

describe('monitoring-services-front App', () => {
  let page: MonitoringServicesFrontPage;

  beforeEach(() => {
    page = new MonitoringServicesFrontPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
