import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActorListAssigmentComponent } from './actor-list-assigment/actor-list-assigment.component';
import { ActorResolutionEvaluationComponent } from './actor-resolution-evaluation/actor-resolution-evaluation.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: ':username',
                component: ActorListAssigmentComponent,
            },
            {
                path: ':username/instrumento',
                children: [
                    {
                        path: ':int_codigo',
                        component: ActorResolutionEvaluationComponent,
                    }
                ]
            },
        ]
    }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
