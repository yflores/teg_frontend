import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ItemModel } from '../models/evaluation.model';

@Injectable()
export class ItemService {
    _urlNgnix = 'http://localhost:5000/items';
    _urlDetail = 'http://localhost:5000/item';

    constructor(private http: Http) {}

    getItems(): Observable<ItemModel[]> {
      return this.http.get(this._urlNgnix)
                 .map(response => response.json() as ItemModel[]);
    }

    getItemsByPregunta(id_Pregunta: number): Observable<ItemModel[]> {
        return this.http.get(`${this._urlDetail}/${id_Pregunta}`)
                   .map(response => response.json() as ItemModel[]);
      }
}
