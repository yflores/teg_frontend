import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment.prod';
import { ComboModel, ItemsComboModel } from '../models/evaluation.model';

@Injectable()
export class ComboService {
    apiurl: string = environment.apiUrl;

    _urlNgnix = 'http://localhost:5000/combos';
    _urlNgnixDetail = 'http://localhost:5000/combo';

    constructor(private http: Http) {}

    getCombos(): Observable<ComboModel[]> {
        return this.http.get(this._urlNgnix)
        .map(response => response.json() as ComboModel[]);
    }

    getNameComboByid(id: number): Observable<string> {
        return this.http.get(`${this._urlNgnixDetail}/${id}`)
        .map(response => response.json() as string);
    }

    getComboByName(name: string): Observable<ComboModel> {
        return this.http.get(`${this._urlNgnixDetail}/findByname/${name}`)
        .map(response => response.json() as ComboModel);
    }

    postCombo(combo: ComboModel): Observable<ComboModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnixDetail}`, JSON.stringify(combo), {headers: headers})
        .map(response => response.json());
    }
}
