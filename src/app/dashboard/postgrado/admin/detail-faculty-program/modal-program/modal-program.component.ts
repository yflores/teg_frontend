import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProgramModel } from '../../../../common-dashboard/models/program.model';
import { ProgramService } from '../../../../common-dashboard/services/program.service';


@Component({
  selector: 'modal-program',
  templateUrl: './modal-program.component.html',
  styleUrls: ['./modal-program.component.css']
})
export class ModalProgramComponent implements OnInit {

  title: string;
  program: ProgramModel;
  idFacultad: number;

  constructor(
    public dialogRef: MatDialogRef<ModalProgramComponent>,
    private programService: ProgramService,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    this.title = this.data.title;
    this.program = this.data.program;
    this.idFacultad = this.data.idFacultad;
    console.log("title ->: ", this.title);
    console.log("idFacultad ->: ", this.idFacultad);
    console.log("Programa ->: ", this.program);
  }

  goBack() {
    this.dialogRef.close();
  }

  saveProgram() {
    console.log("Valor ->: ", this.program);
    this.program.fac_id = this.idFacultad;
    this.programService.postSaveUpdateProgram(this.program).subscribe(facultad => {
      console.log("Programa Guardada...");
      this.dialogRef.close();
    });
  }
}
