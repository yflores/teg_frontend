export class ItemSelectionMultipleModel {
  constructor(
      public slt_id: number,
      public pre_id: number,
      public slt_valor: string
  ) { }
}

export class ItemsComboModel {
    constructor(
        public itcm_id: number,
        public cmb_id: number,
        public cmb_valor: string
    ) { }
}

export class ComboModel {
    constructor(
        public cmb_id: number,
        public cmb_titulo: string,
        public itemsCombo: ItemsComboModel []
    ) { }
}

export class ItemModel {
    constructor(
        public itm_id: number,
        public pre_id: number,
        public itm_data: string,
        public seleccion: string,
        public itm_seleccion: number,
        public combo: string,
        public itm_combo: number,
        public num_combo: number,
        public name_combo: string,
        public observacion: string,
        public itm_observacion: number,
        public itm_combobox: ItemsComboModel [],
        public itm_value: string,
        public itm_value_seleccion: ItemSelectionMultipleModel [],
        public itm_value_combobox: string,
        public itm_value_observacion: string
    ) { }
}

export class PreguntaModel {
    constructor(
        public pre_id: number,
        public eval_id: number,
        public pre_pregunta: string,
        public pre_value: string,
        public sub: string,
        public pre_sub: number,
        public pre_sub_pregunta: ItemModel[]
    ) { }
}

export class EvaluacionModel {
    constructor(
        public eval_id: number,
        public int_codigo: string, //Coincide con el Codigo del Intrumento, Foranea..
        public eval_programa: string,
        public eval_condicionInformante: string,
        public eval_tutor: string,
        public eval_docenteUniCondicion: string,
        public eval_criteriosEscogencia: string,
        public eval_pregunta: PreguntaModel[]
    ) { }
}

export class PreguntaCheckModel {
  constructor(
      public pre_id: number,
      public eval_id: number,
      public pre_pregunta: string,
      public pre_value: string,
      public pre_user: string
  ) { }
}
