import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { PreguntaModel } from '../models/evaluation.model';

@Injectable()
export class PreguntaService {
    _urlNgnix = 'http://localhost:5000/preguntas';
    _urlDetail = 'http://localhost:5000/pregunta';

    constructor(private http: Http) {}

    getPreguntas(): Observable<PreguntaModel[]> {
      return this.http.get(this._urlNgnix)
                 .map(response => response.json() as PreguntaModel[]);
    }

    getPreguntasByEvaluacion(eval_id: number): Observable<PreguntaModel[]> {
        return this.http.get(`${this._urlDetail}/${eval_id}`)
                   .map(response => response.json() as PreguntaModel[]);
      }

      postPreguntas(pregunta: PreguntaModel): Observable<PreguntaModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}`, JSON.stringify(pregunta), {headers: headers})
        .map(response => response.json());
    }

    postPreguntasCheck(preguntas: any, username: string) {
      let headers = new Headers();
      return this.http.post(`${this._urlDetail}/check/${username}`, JSON.stringify(preguntas), {headers: headers})
        .map(response => response.json());
    }

    getPreguntasCheck(code_int: string, username: string) {
      let headers = new Headers();
      return this.http.post(`${this._urlDetail}/check/${username}`, JSON.stringify({code_int, username}), {headers: headers})
        .map(response => response.json());
    }

    postPreguntasCheckByUsernameAndEval(data: any) {
      let headers = new Headers();
      return this.http.post(`${this._urlNgnix}/usernameandcode`, JSON.stringify(data), {headers: headers})
      .map(response => response.json());
    }

    getCantidadPreguntasByEvaluacionId(eval_id: number): Observable<number> {
      return this.http.get(`${this._urlNgnix}/evaluation/${eval_id}`)
                 .map(response => response.json() as number);
    }
}