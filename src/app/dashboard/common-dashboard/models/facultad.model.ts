export class FacultadModel {
    constructor(
        public fac_id: number,
        public fac_nombre: string,
        public fac_descripcion: string
    ) { }
}