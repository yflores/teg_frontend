import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { InstrumentoModel, DataPersonalActorModel, dataInstrumentCheckModel } from '../models/instrumento.models';

@Injectable()
export class InstrumentService {
    _urlNgnix = 'http://localhost:5000/instruments';
    _urlNgnixSave = 'http://localhost:5000/instrument/save';
    _urlDetail = 'http://localhost:5000/instrument';

    constructor(private http: Http) {}

    getInstruments(): Observable<InstrumentoModel[]> {
      return this.http.get(this._urlNgnix)
        .map(response => response.json() as InstrumentoModel[]);
    }

    getInstrumentById(int_id: string): Observable<InstrumentoModel> {
      return this.http.get(`${this._urlDetail}/id/${int_id}`)
        .map(response => response.json() as InstrumentoModel);
    }

    getInstrumentByCode(code: string): Observable<InstrumentoModel> {
        return this.http.get(`${this._urlDetail}/${code}`)
                   .map(response => response.json() as InstrumentoModel);
    }

    getInstrumentsByFacultyId(facultyId: String): Observable<InstrumentoModel[]> {
        return this.http.get(`${this._urlDetail}/faculty/${facultyId}`)
                   .map(response => response.json() as InstrumentoModel[]);
    }

    postInstrument(instrumento: InstrumentoModel): Observable<DataPersonalActorModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnixSave}`, JSON.stringify(instrumento), {headers: headers})
                   .map(response => response.json());
    }

    getInstrumentAssigmentByUser(username: string): Observable<DataPersonalActorModel[]> {
        return this.http.get(`${this._urlNgnix}/sendDataEvaluation/${username}`)
                   .map(response => response.json() as DataPersonalActorModel[]);
    }

    postSendAndSaveDataEvaluationForActor(data: DataPersonalActorModel) {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/sendDataEvaluation`, JSON.stringify(data), {headers: headers})
                   .map(response => response.json());
    }

    posteDataEvaluationCheckForActor(data: dataInstrumentCheckModel) {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/DataEvaluationCheck`, JSON.stringify(data), {headers: headers})
                   .map(response => response.json());
    }

    postDeleteInstrumento(int_id: string) {
        let headers = new Headers();
        return this.http.get(`${this._urlDetail}/delete/${int_id}`, {headers: headers})
                   .map(response => response.json());
    }
}
