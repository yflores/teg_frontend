import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { PreguntaModel, ItemModel } from '../../../../../common-dashboard/models/evaluation.model';
import { ComboSubPreguntaComponent } from '../../../actor-detail/combo-sub-pregunta/combo-sub-pregunta.component';
import { ComboService } from '../../../../../common-dashboard/services/combo.service';

@Component({
  selector: 'modal-edit-pregunta',
  templateUrl: './modal-edit-pregunta.component.html',
  styleUrls: ['./modal-edit-pregunta.component.css']
})
export class ModalEditPreguntaComponent implements OnInit {

  pregunt: PreguntaModel;
  view: number;
  item: ItemModel;
  items: ItemModel[] = [];
  buttonSave = 0;
  modalType: number;
  title: string;

  constructor(
    public dialogRef: MatDialogRef<ModalEditPreguntaComponent>,
    public dialog: MatDialog,
    private comboService: ComboService,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    this.modalType = this.data.modalType;
    this.title =  this.data.title;
    switch (this.modalType) {
      case 0:
        this.pregunt = this.data.pregunta;
        this.view = 0;
        console.log("Prueba de elemento ->: ", this.pregunt);
        this.items = this.pregunt.pre_sub_pregunta;
        this.initializationItems();
        break;
      
        case 1:
          this.item = this.data.subpregunta;
    }
  }

  deleteSubPregunta(element: ItemModel) {
    console.log("Elemento a eliminar ->: ", element);
    let i : number = 0;
    let j : number = 0;
    let encontrado : boolean = false;
    for (let subpregunta of this.pregunt.pre_sub_pregunta) {
      if (subpregunta === element) {
        encontrado = true;
        break;
      } else {
        j ++;
      }
    }
    this.pregunt.pre_sub_pregunta.splice(j, 1);
  }

  addNewSubPreguntas() {
    this.view = 1;
    this.buttonSave = 0;
    console.log("Cambio de Vista...");
  }

  onChangeTypeSubPregunta(searchValue) {
    if(this.item.seleccion !== undefined) {
      if (this.item.seleccion.toString() === "Si") {
        this.item.itm_seleccion = 1;
      } else {
        this.item.itm_seleccion = 0;
      }
    }
    if (this.item.num_combo === undefined) {
      if (this.item.combo !== undefined) {
        if (this.item.combo.toString() === "Si") {
          this.item.itm_combo = 1;
          let idCombo: number;
          const dialogRef = this.dialog.open(ComboSubPreguntaComponent, { data: { idCombo: idCombo } });
          dialogRef.beforeClose().subscribe(idCombo => {
            this.item.num_combo = dialogRef.componentInstance.idCombo;
            console.log("this.item.num_combo -->", this.item.num_combo);
          });
        } else {
          this.item.itm_combo = 0;
        }
      }
    }
    if(this.item.observacion !== undefined) {
      if (this.item.observacion.toString() === "Si") {
        this.item.itm_observacion = 1;
      } else {
        this.item.itm_observacion = 0;
      }
    }
  }

  addNewItem(): void {
    this.comboService.getNameComboByid(this.item.num_combo).subscribe(combo => {
      this.item.name_combo = combo;
      console.log("Miraddd -->: ", combo);
      this.items.push(this.item);
      console.log("Item --->: ", this.items);
      this.initializationItems();
    });
  }

  initializationItems() {
    this.item = new ItemModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined,
      undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
  }

  addNewPreguntaInArray() {
    this.pregunt.pre_sub_pregunta = this.items;
    console.log("Completo ->: ", this.pregunt);
    this.view = 0;
    this.buttonSave = 1;
  }

  finishEditingPreguntas() {
    this.goBack();
  }

  goBack() {
    this.dialogRef.close();
  }
}