import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import {

 } from '@angular/cdk/table';

import 'rxjs/add/operator/switchMap';
import { ComboModel, ItemsComboModel } from '../../../../common-dashboard/models/evaluation.model';
import { ComboService } from '../../../../common-dashboard/services/combo.service';
import { ItemsComboService } from '../../../../common-dashboard/services/itemsCombo.service';
import { CrearComboComponent } from './crear-combo/crear_combo.component';

@Component({
  selector: 'app-combo-sub-pregunta',
  templateUrl: './combo-sub-pregunta.component.html',
  styleUrls: ['./combo-sub-pregunta.component.css']
})
export class ComboSubPreguntaComponent implements OnInit {
  data: string;
  combos: ComboModel[] = [];
  itemsCombo: ItemsComboModel[] = [];
  comboSaved: ComboModel;
  itemsComboSaved: ItemsComboModel;

  constructor(
    public dialogRef: MatDialogRef<ComboSubPreguntaComponent>,
    @Inject(MAT_DIALOG_DATA) public idCombo: number,
    private comboService: ComboService,
    private itemsComboService: ItemsComboService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.comboService.getCombos()
        .subscribe(combos => {
          this.combos = combos;
        });
  }

  onChange(value) {
    this.itemsComboService.getItemsCombo(value.cmb_id)
        .subscribe(itemsCombo => {
          this.itemsCombo = itemsCombo;
        });
  }

  selectedComboItems(combo) {
    console.log("Combo Seleccionado -->: ", combo);
    this.idCombo = combo.cmb_id;
    this.dialogRef.componentInstance.idCombo = this.dialogRef.componentInstance.idCombo;
    this.dialogRef.close();
  }

  createCombo() {
    const dialogCreateCombo = this.dialog.open(CrearComboComponent);
  }

  refresh() {
    console.log("Refrescando combo.");
    this.ngOnInit();
  }

  goBack() {
    console.log("cerrar.");
    this.dialogRef.close();
  }
}
