import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Information } from '../information';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  username: string = Information.getCookieByName("username");

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  gotoList() {
    this.router.navigate(['/dashboard/postgrado/actor/actor-list']);
  }
  gotoLogin() {
    Information.removeCookieByName('username');
    Information.removeCookieByName('role');
    Information.removeCookieByName('facultyId');
    this.router.navigate(['/login']);
  }
}
