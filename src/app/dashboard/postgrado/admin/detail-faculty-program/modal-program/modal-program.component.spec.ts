import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalProgramComponent } from './modal-program.component';

describe('ModalProgramComponent', () => {
  let component: ModalProgramComponent;
  let fixture: ComponentFixture<ModalProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
