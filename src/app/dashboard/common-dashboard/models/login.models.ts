export class LoginModel {
    constructor(
        public id: number,
        public username: string,
        public password: string,
        public role: number,
        public fac_id: number,
        public descripcion: string,
        public token: string
    ) { }
}
