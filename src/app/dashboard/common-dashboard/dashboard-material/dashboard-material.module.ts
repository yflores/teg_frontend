import { NgModule } from '@angular/core';
import { MatToolbarModule, MatMenuModule, MatProgressSpinnerModule, MatIconModule, MatButtonModule, MatSnackBarModule, MatDialogModule } from '@angular/material';

@NgModule({
    imports: [
        MatToolbarModule,
        MatMenuModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatButtonModule,
        MatSnackBarModule,
        MatDialogModule
    ],
    exports: [
        MatToolbarModule,
        MatMenuModule,
        MatProgressSpinnerModule,
        MatIconModule,
        MatButtonModule,
        MatSnackBarModule,
        MatDialogModule
    ]
})
export class DashboardMaterialModule {}
