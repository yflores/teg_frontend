import { NgModule } from '@angular/core';
import { MatDialogModule, MatFormFieldModule, MatInputModule, MatRadioModule } from '@angular/material';
import { routing } from './actor.routing';
import { CommonDashboardModule } from '../../common-dashboard/common-dashboard.module';
import { DashboardMaterialModule } from '../../common-dashboard/dashboard-material/dashboard-material.module';
import { PostgradoMaterialModule } from '../actor/postgrado-material/postgrado-material.module';
import { ActorDetailComponent } from './actor-detail/actor-detail.component';
import { ActorListComponent } from './actor-list/actor-list.component';
import { NewInstrumentoComponent } from './actor-list/new-instrumento/new-instrumento.component';
import { InstrumentService } from "../../common-dashboard/services/instrument.service";
import { EvaluacionService } from '../../common-dashboard/services/evaluacion.service';
import { PreguntaService } from '../../common-dashboard/services/pregunta.service';
import { ItemService } from '../../common-dashboard/services/item.service';
import { ItemsComboService } from '../../common-dashboard/services/itemsCombo.service';
import { GenerationAplicationComponent } from './actor-list/generation-aplication/generation-aplication.component';
import { ComboSubPreguntaComponent } from './actor-detail/combo-sub-pregunta/combo-sub-pregunta.component';
import { ComboService } from '../../common-dashboard/services/combo.service';
import { CrearComboComponent } from './actor-detail/combo-sub-pregunta/crear-combo/crear_combo.component';
import { ConfigAsignacionEvaluationComponent } from './actor-list/config-asignacion-evaluation/config-asignacion-evaluation.component';
import { ModalEvaluationCheckComponent } from './actor-list/modal-evaluation-check/modal-evaluation-check.component';
import { ListEvaluationCheckComponent } from './actor-list/list-evaluation-check/list-evaluation-check.component';
import { EvaluationCheckDetailComponent } from './actor-list/evaluation-check-detail/evaluation.check.detail.component';
@NgModule({
  imports: [
    CommonDashboardModule,
    DashboardMaterialModule,
    PostgradoMaterialModule,
    routing,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule
  ],
  declarations: [
    ActorDetailComponent,
    ActorListComponent,
    NewInstrumentoComponent,
    GenerationAplicationComponent,
    ConfigAsignacionEvaluationComponent,
    ComboSubPreguntaComponent,
    CrearComboComponent,
    ModalEvaluationCheckComponent,
    ListEvaluationCheckComponent,
    EvaluationCheckDetailComponent
  ],
  providers: [
    InstrumentService,
    EvaluacionService,
    PreguntaService,
    ItemService,
    ItemsComboService,
    ComboService,
  ],
  entryComponents: [
    ActorDetailComponent,
    ActorListComponent,
    NewInstrumentoComponent,
    GenerationAplicationComponent,
    ConfigAsignacionEvaluationComponent,
    ComboSubPreguntaComponent,
    CrearComboComponent,
    ModalEvaluationCheckComponent,
    ListEvaluationCheckComponent,
    EvaluationCheckDetailComponent
  ]
})
export class ActorModule {
}
