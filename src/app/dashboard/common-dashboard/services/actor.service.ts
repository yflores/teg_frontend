import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment.prod';
import { ActorModel } from '../models/actor.model';

@Injectable()
export class ActorService {
    apiurl: string = environment.apiUrl;
    
    _urlList = this.apiurl + '/all';
    _urlNgnix = 'http://localhost:5000/actor';
    //_urlDetail = this.apiurl + '/location';

    constructor(private http: Http) {}

    getActor(user_id: number): Observable<ActorModel[]> {
      return this.http.get(`${this._urlNgnix}/${user_id}`)
                 .map(response => response.json() as ActorModel[]);
    }
}
