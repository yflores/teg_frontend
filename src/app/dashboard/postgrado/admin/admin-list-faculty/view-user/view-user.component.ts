import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../common-dashboard/services/user.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { LoginModel } from '../../../../common-dashboard/models/login.models';
import { FacultadModel } from '../../../../common-dashboard/models/facultad.model';
import { FacultadService } from '../../../../common-dashboard/services/facultad.service';
import { MatDialog } from '@angular/material';
import { ModalUserComponent } from './modal-user/modal-user.component';

@Component({
  selector: 'view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  idFacultad: string;
  spinner = true;
  users: LoginModel[] = [];
  facultad: FacultadModel;
  user: LoginModel;

  constructor(
    private userService: UserService,
    private facultadService: FacultadService,
    private route: ActivatedRoute,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) => this.idFacultad = params.get('id_facultad'))
    .subscribe(idFacultad => {
        this.userService.getUserByFacultadId(idFacultad)
        .subscribe(users => {
          this.users = users;
        });
        this.facultadService.getFacultadById(parseInt(this.idFacultad)).subscribe(facultad => {
          this.facultad = facultad;
        });
        this.spinner = false;
    });
  }

  openModalNewUser() {
    const dialogRef = this.dialog.open(ModalUserComponent, {
      height: '350px',
      width: '500px',
      data: {
        title: "Nuevo Usuario",
        user: new LoginModel(0, undefined, undefined, undefined, undefined, undefined, undefined),
        idFacultad: this.idFacultad
      }
    });
    dialogRef.beforeClose().subscribe(user => {
     this.ngOnInit();
    });
  }

  openModalEditUser(user: LoginModel) {
    const dialogRef = this.dialog.open(ModalUserComponent, {
      height: '350px',
      width: '500px',
      data: {
        title: "Editar Usuario",
        user: user,
        idFacultad: this.idFacultad
      }
    });
    dialogRef.beforeClose().subscribe(idCombo => {
     this.ngOnInit();
    });
  }

  deleteUser(user: LoginModel) {
    console.log("Usuario ->: ", user);
    this.userService.postdeleteUser(user).subscribe(user => {
      console.log("Usuario Eliminado...");
      this.ngOnInit();
    });
  }
}
