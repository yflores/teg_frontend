import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {InstrumentService} from '../../../../common-dashboard/services/instrument.service';
import {InstrumentoModel} from '../../../../common-dashboard/models/instrumento.models';
import {EvaluacionService} from '../../../../common-dashboard/services/evaluacion.service';
import {ComboModel, EvaluacionModel, ItemModel, PreguntaModel} from '../../../../common-dashboard/models/evaluation.model';
import {PreguntaService} from '../../../../common-dashboard/services/pregunta.service';
import {ItemService} from '../../../../common-dashboard/services/item.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ComboSubPreguntaComponent} from '../../actor-detail/combo-sub-pregunta/combo-sub-pregunta.component';
import {MatDialog} from '@angular/material';
import { ModalEditPreguntaComponent } from '../new-instrumento/modal-edit-pregunta/modal-edit-pregunta.component';

@Component({
  selector: 'edit-instrumento',
  templateUrl: './edit-instrumento.component.html',
  styleUrls: ['./edit-instrumento.component.css']
})
export class EditInstrumentoComponent implements OnInit {

  instrument: InstrumentoModel;
  evaluation: EvaluacionModel;
  spinner = false;
  form: FormGroup;
  pregunta: PreguntaModel;
  item: ItemModel;
  items: ItemModel[];
  combo: ComboModel;
  pregunts: PreguntaModel[];

  constructor(
    private route: ActivatedRoute,
    private instrumentService: InstrumentService,
    private evaluationService: EvaluacionService,
    private preguntService: PreguntaService,
    private itemService: ItemService,
    fb: FormBuilder,
    public dialog: MatDialog
  ) {
    this.form = fb.group({
      profesor: ['', Validators.required],
      participante: ['', Validators.required],
      empleado: ['', Validators.required],
      egresado: ['', Validators.required]
    });
  }
  public editor;
  public editorOptions = {
    placeholder: 'Ingrese una descripción a la Evaluación...'
  };

  ngOnInit() {
    this.resetPregunta();
    this.route.paramMap
      .switchMap((params: ParamMap) => this.instrumentService.getInstrumentById(params.get('int_id')))
      .subscribe(instrument => {
        this.instrument = instrument;
        this.evaluationService.getEvaluacionByCodeInstrument(this.instrument.int_codigo)
          .subscribe(evaluation => {
            this.evaluation = evaluation;
            this.preguntService.getPreguntasByEvaluacion(this.evaluation.eval_id)
              .subscribe(pregunts => {
                this.evaluation.eval_pregunta = pregunts;
                this.evaluation.eval_pregunta.forEach(pregunt => {
                  this.itemService.getItemsByPregunta(pregunt.pre_id)
                    .subscribe(items => {
                      pregunt.pre_sub_pregunta = items;
                    });
                });
                console.log('Instrumento a Editar ->: ', this.instrument);
                console.log('Evaluacion a Editar ->: ', this.evaluation);
                this.form.get('profesor').setValue(this.instrument.int_profesor);
                this.form.get('participante').setValue(this.instrument.int_participante);
                this.form.get('empleado').setValue(this.instrument.int_empleado);
                this.form.get('egresado').setValue(this.instrument.int_egresado);
                this.spinner = true;
              });
          });
      });
  }

  resetPregunta () {
    this.pregunta = new PreguntaModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined);

    this.item = new ItemModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined,
      undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
  }

  onChange(searchValue) {
    console.log(searchValue);
  }

  onEditorBlured(quill) {
    //console.log('editor blur!', quill);
  }

  onEditorFocused(quill) {
    //console.log('editor focus!', quill);
  }

  onEditorCreated(quill) {
    this.editor = quill;
    //console.log('quill is ready! this is current quill instance object', quill);
  }

  onContentChanged({ quill, html, text }) {
    console.log('quill content is changed!', quill, html, text);
    this.instrument.int_descripcion = html;
  }

  onChangePregunta(searchValue) {
    if (this.pregunta.sub.toString() === 'Si') {
      this.pregunta.pre_sub = 1;
    } else {
      this.pregunta.pre_sub = 0;
    }
  }

  setypePerson(typePerson: String, value: number) {
    if (typePerson === "int_profesor") {
      this.instrument.int_profesor = value;
    } else {
      if (typePerson === "int_participante") {
        this.instrument.int_participante = value;
      } else {
        if (typePerson === "int_egresado") {
          this.instrument.int_egresado = value;
        } else {
          if (typePerson === "int_empleado") {
            this.instrument.int_empleado = value;
          }
        }
      }
    }
  }

  resetRadio(event: any, value: string) {
    const typePerson = 'int_' + value;
    let numberType = 1;
    if (this.form.get(value).value === event.target.value){
        this.form.get(value).setValue('false');
        numberType = 0;
    }
    this.setypePerson(typePerson, numberType);
  }

  onChangeTypeSubPregunt(searchValue) {
    if (this.item.seleccion !== undefined) {
      if (this.item.seleccion.toString() === 'Si') {
        this.item.itm_seleccion = 1;
      } else {
        this.item.itm_seleccion = 0;
      }
    }
    if (this.item.num_combo === undefined) {
      if (this.item.combo !== undefined) {
        if (this.item.combo.toString() === 'Si') {
          this.item.itm_combo = 1;
          let idCombo: number;
          const dialogRef = this.dialog.open(ComboSubPreguntaComponent, { data: { idCombo: idCombo } });
          dialogRef.beforeClose().subscribe(id_combo => {
            this.item.num_combo = dialogRef.componentInstance.idCombo;
            console.log('this.item.num_combo -->', this.item.num_combo);
          });
        } else {
          this.item.itm_combo = 0;
        }
      }
    }
    if (this.item.observacion !== undefined) {
      if (this.item.observacion.toString() === 'Si') {
        this.item.itm_observacion = 1;
      } else {
        this.item.itm_observacion = 0;
      }
    }
    console.log('pregunta.pre_sub -->: ', this.pregunta);
    console.log('item.itm_seleccion -->', this.item.itm_seleccion);
    console.log('item.itm_combo -->', this.item.itm_combo);
    console.log('item.itm_observacion -->', this.item.itm_observacion);
  }

  openModalEditSubPregunta(subpregunta) {
    const dialogRef = this.dialog.open(ModalEditPreguntaComponent, {
      height: '200px',
      width: '1005px',
      data: {
        subpregunta: subpregunta,
        title: 'Editar subpregunta',
        modalType: 1
      }
    });
    dialogRef.beforeClose().subscribe(idCombo => {
     console.log('Modal Cerrada.');
    });
  }
}
