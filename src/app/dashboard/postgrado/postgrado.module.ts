import { NgModule } from '@angular/core';

import { CommonDashboardModule } from '../common-dashboard/common-dashboard.module';
import { DashboardMaterialModule } from '../common-dashboard/dashboard-material/dashboard-material.module';
import { PostgradoMaterialModule } from './postgrado-material/postgtado-material.module';
import { ActorModule } from './actor/actor.module';
import { AdminModule } from './admin/admin.module';
import { routing } from './postgrado.routing';

import { PostgradoComponent } from './postgrado.component';
import { InstrumentService } from '../common-dashboard/services/instrument.service';

@NgModule({
  imports: [
    CommonDashboardModule,
    DashboardMaterialModule,
    PostgradoMaterialModule,
    ActorModule,
    AdminModule,
    routing,
  ],
  declarations: [
    PostgradoComponent,
  ],
  providers: [
    InstrumentService
  ]
})
export class PostgradoModule {}
