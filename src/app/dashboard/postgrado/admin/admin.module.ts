import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostgradoMaterialModule } from '../actor/postgrado-material/postgrado-material.module';
import { AdminListFacultyComponent } from './admin-list-faculty/admin-list-faculty.component';
import { routing } from './admin.routing';
import { FacultadService } from '../../common-dashboard/services/facultad.service';
import { MatProgressSpinnerModule, MatInputModule, MatFormFieldModule, MatTooltipModule, MatDialogModule } from '@angular/material';
import { DashboardMaterialModule } from '../../common-dashboard/dashboard-material/dashboard-material.module';
import { CommonDashboardModule } from '../../common-dashboard/common-dashboard.module';
import { ProgramService } from '../../common-dashboard/services/program.service';
import { DetailFacultyProgramComponent } from './detail-faculty-program/detail-faculty-program.component';
import { RouterModule } from '@angular/router';
import { ModalCreateFacultadComponent } from './admin-list-faculty/modal-create-facultad/modal-create-facultad.component';
import { ModalProgramComponent } from './detail-faculty-program/modal-program/modal-program.component';
import { ViewUserComponent } from './admin-list-faculty/view-user/view-user.component';
import { UserService } from '../../common-dashboard/services/user.service';
import { ModalUserComponent } from './admin-list-faculty/view-user/modal-user/modal-user.component';

@NgModule({
  imports: [
    CommonModule,
    routing,
    RouterModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    CommonDashboardModule,
    DashboardMaterialModule,
    PostgradoMaterialModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [
    FacultadService, 
    ProgramService,
    UserService
  ],
  declarations: [
    AdminListFacultyComponent,
    DetailFacultyProgramComponent,
    ModalCreateFacultadComponent,
    ModalProgramComponent,
    ViewUserComponent,
    ModalUserComponent
  ],
  entryComponents: [
    AdminListFacultyComponent,
    DetailFacultyProgramComponent,
    ModalCreateFacultadComponent,
    ModalProgramComponent,
    ViewUserComponent,
    ModalUserComponent
  ],
})
export class AdminModule { }
