import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, Params } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';

import 'rxjs/add/operator/switchMap';

import { ActorModel } from '../../../common-dashboard/models/actor.model';

import { ActorService } from '../../../common-dashboard/services/actor.service';
import { EvaluacionModel, PreguntaModel, ItemModel, ItemsComboModel } from '../../../common-dashboard/models/evaluation.model';
import { InstrumentoModel } from '../../../common-dashboard/models/instrumento.models';
import { InstrumentService } from '../../../common-dashboard/services/instrument.service';
import { EvaluacionService } from '../../../common-dashboard/services/evaluacion.service';
import { PreguntaService } from '../../../common-dashboard/services/pregunta.service';
import { ItemService } from '../../../common-dashboard/services/item.service';
import { forEach } from '@angular/router/src/utils/collection';
import { ItemsComboService } from '../../../common-dashboard/services/itemsCombo.service';

@Component({
  selector: 'app-actor-detail',
  templateUrl: './actor-detail.component.html',
  styleUrls: ['./actor-detail.component.css']
})
export class ActorDetailComponent implements OnInit {

  instrument: InstrumentoModel;
  panelOpenState = false;
  codigo: string;
  evaluacion: EvaluacionModel;
  preguntas: PreguntaModel[] = [];
  items: ItemModel[] = [];
  spinnerOff = 1;
  itemsCombo: ItemsComboModel[] = [];

  checked = false;
  indeterminate = false;
  labelPosition = 'after';
  disabled = false;

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private matDialog: MatDialog,
    private intrumentService: InstrumentService,
    private evaluacionService: EvaluacionService,
    private preguntaService: PreguntaService,
    private itemService: ItemService,
    private itemsComboSevice: ItemsComboService
  ) { }

  ngOnInit() {
    this.route.paramMap
    .switchMap((params: ParamMap) => this.codigo = params.get('int_codigo'))
    .subscribe(codigo => {
        this.codigo.concat(codigo);
    });

    this.evaluacionService.getEvaluacionByCodeInstrument(this.codigo)
        .subscribe(evaluacion => {
          this.evaluacion = evaluacion;

          this.preguntaService.getPreguntasByEvaluacion(this.evaluacion.eval_id)
              .subscribe(preguntas => {
                this.preguntas = preguntas;

                  this.preguntas.forEach(element => {
                    this.itemService.getItemsByPregunta(element.pre_id)
                        .subscribe(items => {
                          this.items = items;
                          this.items.forEach( itm => {
                              if(element.pre_id === itm.pre_id) {
                                element.pre_sub_pregunta = this.items.slice();
                              }
                              console.log("value itm --->: ", itm.itm_combo);
                              if (itm.itm_combo > 1) {
                                this.itemsComboSevice.getItemsCombo(itm.itm_combo)
                                .subscribe(itemsCombo => {
                                  this.itemsCombo = itemsCombo;
                                  console.log("Epale mira el valor --->: ", this.itemsCombo);
                                  if(itm.itm_combo !== 0) {
                                    console.log("Epa hijo de Dios...");
                                    itm.itm_combobox = this.itemsCombo.slice();
                                  }
                                });
                              }
                          });
                        });
                      });
              });
        });

    this.intrumentService.getInstrumentByCode(this.codigo)
        .subscribe(instrument => {
          this.instrument = instrument;
        });
        this.spinnerOff = 0;
  }

  onSearchChange(searchValue) {
    console.log(searchValue);
  }

  goBack(country: string): void {
    //this.navactor.back();
  }

  accept(): void {
    //this.navactor.back();
  }

  /*openExtendedServerDetail(server: ServerModel, actor: actorModel) {
    this.matDialog
        .open(ExtendedServerDetailComponent, { data: [server, actor] });
  }*/
}
