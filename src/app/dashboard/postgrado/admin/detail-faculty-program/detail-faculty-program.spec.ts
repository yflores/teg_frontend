import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminListFacultyComponent } from './detail-faculty-program.component';

describe('AdminListFacultyComponent', () => {
  let component: AdminListFacultyComponent;
  let fixture: ComponentFixture<AdminListFacultyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminListFacultyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminListFacultyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
