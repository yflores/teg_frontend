import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../dashboard/common-dashboard/services/login.service';
import { LoginModel } from '../dashboard/common-dashboard/models/login.models';
import { Information } from '../information';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ModalComponent } from '../dashboard/common-dashboard/modal/modal.component';
import { Subscription } from 'rxjs';
import { timer } from 'rxjs/observable/timer';
import { map } from 'rxjs/operators';
import { UserService } from '../dashboard/common-dashboard/services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: LoginModel;
  salida: any;
  msj: string;
  typeModal: String = "";
  title: String = "";
  message: String = "";
  routeModalNext: String  = "";
  routeModaPrevious: String  = "";
  sessionActive: LoginModel;
  timerSubscription: Subscription;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private dialog: MatDialog,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.loginData = new LoginModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined);
  }

  validateLogin(login: LoginModel): void {
    if (login.username === undefined && login.password === undefined) {
      login.username = '';
      login.password = '';
    }

    this.loginService.putLoginData(login).subscribe(salida => {
      this.salida = salida;
      console.log("Salida -->: ", this.salida)
      if (this.salida.role === 1) {
        Information.createCookieResponseQueue(`username = ${this.salida.username}`);
        Information.createCookieResponseQueue(`role =  ${this.salida.role}`);
        Information.createCookieResponseQueue(`facultyId = ${this.salida.facultyId}`);
        Information.createVariableSessionStorage('tokenSession', `${this.salida.token}`);
        Information.createVariableSessionStorage('expirationDateTokenSession', this.changeMinutesInDateExpirationSession().toString());

        this.timerSubscription = timer(0, 10000).pipe(
          map(() => {
            this.userService.getUserByUsername(this.salida.username)
            .subscribe(user => {
              this.loginService.getActiveSession(user.id)
                .subscribe(sessionActive => {
                  this.sessionActive = sessionActive;
                  console.log("List session active -->: ", sessionActive);

                  let tkn_value = Information.getVariableSessionStorage('tokenSession');
                  if (tkn_value === this.sessionActive.token) {
                    let expirationDateTokenSession = Information.getVariableSessionStorage('expirationDateTokenSession');

                    if (new Date().toString() >= expirationDateTokenSession) {
                     //Agregar modal o validación para extender la sesion.

                      Information.removeSessionStorageByName('tokenSession');
                      Information.removeSessionStorageByName('expirationDateTokenSession');
                      this.router.navigate(['/login']);
                    }
                  }
                });
            });
          })
        ).subscribe();

        this.router.navigate(['/dashboard/postgrado/actor', this.salida.username]);
      }else{
        if (this.salida.role === 2) {
          Information.createCookieResponseQueue(`username = ${this.salida.username}`);
          Information.createCookieResponseQueue(`role =  ${this.salida.role}`);
          Information.createCookieResponseQueue(`facultyId = ${this.salida.facultyId}`);
          this.router.navigate(['/dashboard/postgrado/actor-evaluation', this.salida.username]);
        } else {
            login.username = '';
            login.password = '';
            login.role = 0;

            this.typeModal = "Inicio de sesión";
            this.title = "Error de Autenticación";
            this.message = "Error en el usuario o contraseña, verifique e intente nuevamente"
            this.routeModalNext = "";
            this.routeModaPrevious = "/login";

            const dialogConfig = new MatDialogConfig();

            dialogConfig.data = {
              typeModal: this.typeModal,
              title: this.title,
              message: this.message,
              routeModalNext: this.routeModalNext,
              disableClose: true
            };
            dialogConfig.width = '400px';

            const dialogRef = this.dialog.open(ModalComponent, dialogConfig);
        }
      }
    });
  }

  changeMinutesInDateExpirationSession() {
    var date = new Date();
    var addMinutesSession = 3;
    var minutes = date.getMinutes();

    date.setMinutes(minutes + addMinutesSession);

    return date;
  }
}

