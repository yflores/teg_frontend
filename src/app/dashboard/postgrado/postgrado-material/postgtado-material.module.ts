import { NgModule } from '@angular/core';
import { MatTabsModule, MatDialogModule } from '@angular/material';

@NgModule({
    imports: [
        MatTabsModule,
        MatDialogModule
    ],
    exports: [
        MatTabsModule,
        MatDialogModule
    ]
})
export class PostgradoMaterialModule {}
