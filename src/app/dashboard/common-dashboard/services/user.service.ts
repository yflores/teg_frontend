import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment.prod';
import { LoginModel } from '../models/login.models';

@Injectable()
export class UserService {
    apiurl: string = environment.apiUrl;

    _urlNgnix = 'http://localhost:5000/user';

    constructor(private http: Http) {}

    getUserByFacultadId(id_facultad: string): Observable<LoginModel[]> {
        return this.http.get(`${this._urlNgnix}/usersbyfaculty/${id_facultad}`)
        .map(response => response.json() as LoginModel[]);
    }

    getUserByUsername(username: string): Observable<LoginModel> {
      return this.http.get(`${this._urlNgnix}/usersbyusername/${username}`)
      .map(response => response.json() as LoginModel);
  }

    postSaveUpdateUser(user: LoginModel): Observable<LoginModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/saveupdateuser`, JSON.stringify(user), {headers: headers})
                   .map(response => response.json());
    }

    postdeleteUser(user: LoginModel): Observable<LoginModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/deleteuser`, JSON.stringify(user), {headers: headers})
                   .map(response => response.json());
    }
}
