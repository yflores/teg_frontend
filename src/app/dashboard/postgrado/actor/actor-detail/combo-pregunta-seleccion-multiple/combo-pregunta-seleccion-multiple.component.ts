import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ItemSelectionMultipleModel } from '../../../../common-dashboard/models/evaluation.model';

@Component({
  selector: 'combo-pregunta-seleccion-multiple',
  templateUrl: './combo-pregunta-seleccion-multiple.component.html',
  styleUrls: ['./combo-pregunta-seleccion-multiple.component.css']
})
export class ComboPreguntaSeleccionMultipleComponent implements OnInit {

  valueSeleccion: ItemSelectionMultipleModel;

  constructor(
    public dialogRef: MatDialogRef<ComboPreguntaSeleccionMultipleComponent>,
    @Inject(MAT_DIALOG_DATA) public itemSeleccionMultiple:  ItemSelectionMultipleModel[],
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.itemSeleccionMultiple = new Array();
    this.inicializacion();
  }

  goBack() {
    this.dialogRef.close();
  }

  inicializacion () {
    this.valueSeleccion = new ItemSelectionMultipleModel(undefined, undefined, undefined);
  }

  onChange(value) {
    console.log("value -->: ", value);
  }

  onSaveMemory (valueSeleccion) {
    this.itemSeleccionMultiple.push(valueSeleccion);
    console.log("Items de Seleccion -->: ", this.itemSeleccionMultiple);
    this.inicializacion();
  }

  deleteItem (value) {
    let i = 0;
    let encontrado = false;
    for (const item of this.itemSeleccionMultiple) {
      if (item.slt_valor === value) {
        encontrado = true;
        break;
      } else {
        i ++;
      }
    }
    this.itemSeleccionMultiple.splice(i, 1);
  }
}
