import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { MatDialog, MatSnackBar } from '@angular/material';

import { InstrumentService } from '../../../common-dashboard/services/instrument.service';
import { InstrumentoModel } from '../../../common-dashboard/models/instrumento.models';
import { EvaluacionService } from '../../../common-dashboard/services/evaluacion.service';
import { ModalEvaluationCheckComponent } from './modal-evaluation-check/modal-evaluation-check.component';
import { Information } from '../../../../information';

@Component({
  selector: 'app-actor-list',
  templateUrl: './actor-list.component.html',
  styleUrls: ['./actor-list.component.css']
})
export class ActorListComponent implements OnInit {
  spinnerOff = true;
  instruments: InstrumentoModel[] = [];
  matDialog: any;
  evalsCheck: any[];
  role: string = Information.getCookieByName('role');

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router,
     private instrumentService: InstrumentService,
    private evaluationService: EvaluacionService
  ) { }

  ngOnInit() {
    this.evaluationService.getListEvaluationsCheck()
    .subscribe(evalsCheck => {
      this.evalsCheck = evalsCheck;
      console.log('EvalsCheck ->: ', this.evalsCheck);
      if (this.evalsCheck.length > 0) {
        const dialogRef = this.dialog.open(ModalEvaluationCheckComponent);
      }
    });

    this.instrumentService.getInstrumentsByFacultyId(Information.getCookieByName('facultyId'))
    .subscribe(instruments => {
      this.instruments = instruments;
      console.log('instrumentos ->: ', this.instruments);
      this.spinnerOff = false;
    });
  }

  openNewEvaluation() {
    this.router.navigate(['/dashboard/postgrado/actor/actor-list/new-instrumento']);
  }

  openMaster() {
    this.router.navigate(['/dashboard/postgrado/administrator/main']);
  }

  openGenerationEvaluationActors() {
    this.router.navigate(['/dashboard/postgrado/actor/actor-list/generation-aplication']);
  }

  viewListEvaluationCheck() {
    this.router.navigate(['/dashboard/postgrado/actor/evaluations-check']);
  }

  editInstrumentComplete(int_id) {
    this.router.navigate(['/dashboard/postgrado/actor/edit-instrument', int_id]);
  }

  deleteInstrumentComplete(int_id) {
    this.instrumentService.postDeleteInstrumento(int_id)
    .subscribe(instrument => {
      console.log('Intrumento -> Evaluacion -> Preguntas -> Subpreguntas (Eliminado OK).');
      this.instrumentService.getInstrumentsByFacultyId(Information.getCookieByName('facultyId'))
      .subscribe(instruments => {
        this.instruments = instruments;
        console.log('instrumentos ->: ', this.instruments);
        this.spinnerOff = false;
      });
    });
  }
}
