export class ProgramModel {
    constructor(
        public pro_id: number,
        public pro_nombre: string,
        public fac_id: number,
        public pro_descripcion: string
    ) { }
}