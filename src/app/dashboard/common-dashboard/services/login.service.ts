import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { environment } from '../../../../environments/environment.prod';
import { LoginModel } from '../models/login.models';

@Injectable()
export class LoginService {
    apiurl: string = environment.apiUrl;

    _urlLogin = `${this.apiurl}/security`;

    constructor(private http: Http) { }

    putLoginData(loginData: LoginModel): Observable<LoginModel[]> {
        const headers = new Headers();
        return this.http.post(`${this._urlLogin}`, JSON.stringify(loginData), { headers: headers })
            .map(response => response.json());
    }

    getFacultyByUsername(username: string): Observable<number> {
        return this.http.get(`${this._urlLogin}/faculty/${username}`)
                   .map(response => response.json() as number);
    }

    getActiveSession(username: number): Observable<LoginModel> {
      return this.http.get(`${this._urlLogin}/activeSession/${username}`)
                   .map(response => response.json() as LoginModel);
    }
}
