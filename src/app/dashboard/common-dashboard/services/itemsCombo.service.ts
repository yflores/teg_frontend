import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment.prod';
import { ItemsComboModel } from '../models/evaluation.model';

@Injectable()
export class ItemsComboService {
    apiurl: string = environment.apiUrl;

    _urlList = this.apiurl + '/all';
    _urlNgnix = 'http://localhost:5000/items-combo';
    //_urlDetail = this.apiurl + '/location';

    constructor(private http: Http) {}

    getItemsCombo(cmb_id: number): Observable<ItemsComboModel[]> {
      return this.http.get(`${this._urlNgnix}/${cmb_id}`)
                 .map(response => response.json() as ItemsComboModel[]);
    }

    postItemsCombo(itemsCombo: ItemsComboModel, idCombo: number): Observable<ItemsComboModel> {
        let headers = new Headers();
        itemsCombo.cmb_id = idCombo;
        return this.http.post(`${this._urlNgnix}`, JSON.stringify(itemsCombo), {headers: headers})
        .map(response => response.json());
    }
}
