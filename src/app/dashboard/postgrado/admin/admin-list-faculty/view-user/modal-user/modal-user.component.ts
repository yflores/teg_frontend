import { Component, OnInit, Inject } from '@angular/core';
import { LoginModel } from '../../../../../common-dashboard/models/login.models';
import {MatDialogRef, MAT_DIALOG_DATA, MatDialog} from '@angular/material';
import { UserService } from '../../../../../common-dashboard/services/user.service';

@Component({
  selector: 'modal-user',
  templateUrl: './modal-user.component.html',
  styleUrls: ['./modal-user.component.css']
})
export class ModalUserComponent implements OnInit {

  title: string;
  user: LoginModel;
  idFacultad: number;

  constructor(
    public dialogRef: MatDialogRef<ModalUserComponent>,
    private userService: UserService,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    this.title = this.data.title;
    this.user = this.data.user;
    this.idFacultad = this.data.idFacultad;
    console.log("title ->: ", this.title);
    console.log("idFacultad ->: ", this.idFacultad);
    console.log("user ->: ", this.user);
  }

  goBack() {
    this.dialogRef.close();
  }

  saveUpdateUser() {
    console.log("Valor ->: ", this.user);
    this.user.fac_id = this.idFacultad;
    this.user.role = 1;
    this.userService.postSaveUpdateUser(this.user).subscribe(facultad => {
      console.log("Usuario Guardado...");
      this.dialogRef.close();
    });
  }
}
