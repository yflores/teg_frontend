import { ActorDetailComponent } from './actor-detail/actor-detail.component';
import { ActorListComponent } from './actor-list/actor-list.component';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewInstrumentoComponent } from './actor-list/new-instrumento/new-instrumento.component';
import { GenerationAplicationComponent } from './actor-list/generation-aplication/generation-aplication.component';
import { ListEvaluationCheckComponent } from './actor-list/list-evaluation-check/list-evaluation-check.component';
import { EvaluationCheckDetailComponent } from './actor-list/evaluation-check-detail/evaluation.check.detail.component';
import {EditInstrumentoComponent} from './actor-list/edit-instrumento/edit-instrumento.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: ':user_id',
                component: ActorListComponent,
            },
            {
                path: ':evaluation/user_id',
                component: ActorListComponent,
            },
            {
                path: ':user_id/instrumento',
                children: [
                    {
                        path: ':int_codigo',
                        component: ActorDetailComponent,
                    }
                ]
            },
            {
                path: ':/new-instrumento',
                component: NewInstrumentoComponent,
            },
            {
                path: ':/generation-aplication',
                component: GenerationAplicationComponent,
            },
            {
                path: ':/evaluations-check',
                component: ListEvaluationCheckComponent,
                children: []
            },
            {
              path: ':/evaluation',
              children: [
                  {
                    path: ':eval-check',
                    component: EvaluationCheckDetailComponent
                  }
                ]
            },
          {
            path: ':/edit-instrument',
            children: [
              {
                path: ':int_id',
                component: EditInstrumentoComponent
              }
            ]
          }
        ]
    }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
