import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FacultadModel } from '../../../../common-dashboard/models/facultad.model';
import { FacultadService } from '../../../../common-dashboard/services/facultad.service';

@Component({
  selector: 'modal-create-facultad',
  templateUrl: './modal-create-facultad.component.html',
  styleUrls: ['./modal-create-facultad.component.css']
})
export class ModalCreateFacultadComponent implements OnInit {

  title: string;
  facultad: FacultadModel;

  constructor(
    public dialogRef: MatDialogRef<ModalCreateFacultadComponent>,
    private facultadService: FacultadService,
    @Inject(MAT_DIALOG_DATA) private data: any
  ) { }

  ngOnInit() {
    this.title = this.data.title;
    this.facultad = this.data.facultad;
    console.log("title ->: ", this.title);
    console.log("Facultad ->: ", this.facultad);
  }

  goBack() {
    this.dialogRef.close();
  }

  saveFacultad() {
    console.log("Valor ->: ", this.facultad);
    this.facultadService.postSaveUpdateFacultad(this.facultad).subscribe(facultad => {
      console.log("Facultad Guardada...");
      this.dialogRef.close();
    });
  }
}
