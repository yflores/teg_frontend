import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router, Params } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';

import 'rxjs/add/operator/switchMap';

import { InstrumentoModel } from '../../../../common-dashboard/models/instrumento.models';
import { InstrumentService } from '../../../../common-dashboard/services/instrument.service';
import { EvaluacionService } from '../../../../common-dashboard/services/evaluacion.service';
import { PreguntaService } from '../../../../common-dashboard/services/pregunta.service';
import { ItemService } from '../../../../common-dashboard/services/item.service';
import { ItemsComboService } from '../../../../common-dashboard/services/itemsCombo.service';
import { EvaluacionModel, PreguntaModel, ItemModel, ItemsComboModel } from '../../../../common-dashboard/models/evaluation.model';
import { ProgramModel } from '../../../../common-dashboard/models/program.model';
import { ProgramService } from '../../../../common-dashboard/services/program.service';

@Component({
  selector: 'app-evaluation-check-detail',
  templateUrl: './evaluation.check.detail.component.html',
  styleUrls: ['./evaluation.check.detail.component.css']
})

export class EvaluationCheckDetailComponent implements OnInit {
  instrument: InstrumentoModel;
  panelOpenState = false;
  value: string;
  codigo: string;
  username: string;
  isUsername: boolean;
  evaluacion: EvaluacionModel;
  preguntas: PreguntaModel[] = [];
  items: ItemModel[] = [];
  spinnerOff = 1;
  itemsCombo: ItemsComboModel[] = [];
  eval_id: number;
  data: any;
  preguntasCheck: any[];
  cantPreguntasEval: number;
  arrayPreguntasChecks: any[][];
  indiceView: number;
  arrayOut: any;
  subPreguntas: any[];
  numberIndex : number = 0;
  numberMax : number = 0;
  cantPreguntas: number;
  definitive: any[];
  preguntasMatriz: any[] = [];
  program: ProgramModel;

  checked = false;
  indeterminate = false;
  labelPosition = 'after';
  disabled = false;

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private matDialog: MatDialog,
    private intrumentService: InstrumentService,
    private evaluacionService: EvaluacionService,
    private preguntaService: PreguntaService,
    private itemService: ItemService,
    private itemsComboSevice: ItemsComboService,
    private programService: ProgramService
  ) { }

  ngOnInit() {
    this.cantPreguntas = 0;

    this.route.paramMap
    .switchMap((params: ParamMap) => this.value = params.get('eval-check'))
    .subscribe(value => {
        this.value.concat(value);
    });

    this.getCodigoAndUsername();
    this.evaluacionService.getEvaluationsCheckByUsername(this.username)
      .subscribe(evalCheck => {
        this.programService.getProgramById(evalCheck.pro_id)
          .subscribe(program => {
            this.program = program;
            console.log("Programa -->: ", this.program);
          });
      });

    this.evaluacionService.getEvaluacionByCodeInstrument(this.codigo)
      .subscribe(evaluacion => {
        this.eval_id = evaluacion.eval_id;
        this.data = {
          "username": this.username,
          "eval_id": this.eval_id
        };

        this.preguntaService.getCantidadPreguntasByEvaluacionId(this.eval_id)
        .subscribe(cantidadPreguntas => {
          this.cantPreguntas = cantidadPreguntas;
        });

        this.preguntaService.postPreguntasCheckByUsernameAndEval(this.data)
          .subscribe(outData => {
            this.preguntasCheck = outData;
            this.preguntaService.getPreguntasByEvaluacion(this.eval_id)
              .subscribe(cantPreguntasEval => {
                this.cantPreguntasEval = cantPreguntasEval.length;
                let indice:number = 0;
                let i: number = 0;
                let j: number = 0;
                this.arrayPreguntasChecks = [];
                this.arrayPreguntasChecks[i] = [];
                this.preguntasCheck.forEach(element => {
                  element.sub_pregunta = this.convertJsonSubPregunt(element.value);
                  if (element.sub_pregunta.length > 0) {
                    element.subPreguntNumber = 1;
                  } else {
                    element.subPreguntNumber = 0;
                  }

                  if (indice < this.cantPreguntasEval) {
                    this.arrayPreguntasChecks[i][j] = element;
                    j ++;
                    indice ++;
                  }
                  if (indice === this.cantPreguntasEval) {
                    j = 0;
                    i ++;
                    indice = 0;
                    this.arrayPreguntasChecks[i] = [];
                  }
                });
                this.arrayPreguntasChecks = this.validationArrayPreguntasCheck(this.arrayPreguntasChecks);
                //this.numberMax = (this.arrayPreguntasChecks.length - 1);
                //this.arrayOut = this.arrayPreguntasChecks[this.numberIndex];
                this.arrayOut = this.preguntasCheck;
                console.log("Evaluacion Resuelta ->: ", this.arrayOut);
                let totalIter = (this.arrayOut.length / this.cantPreguntas);
                let k = 0;
                this.definitive = new Array();
                for (let i = 0; i < totalIter; i++) {
                  let array = new Array();
                  for (let j = 0; j < this.cantPreguntas; j++) {
                    array.push(this.arrayOut[k]);
                    k = (k + 1);
                  }
                  this.definitive.push(array);
                }
                console.log("Finalmente -->: ", this.definitive);
                this.preguntasMatriz = this.definitive[0];
                //aqui lo demas..
              });
          });
      });

    this.evaluacionService.getEvaluacionByCodeInstrument(this.codigo)
        .subscribe(evaluacion => {
          this.evaluacion = evaluacion;
          this.preguntaService.getPreguntasByEvaluacion(this.evaluacion.eval_id)
              .subscribe(preguntas => {
                this.preguntas = preguntas;
                  this.preguntas.forEach(element => {
                    this.itemService.getItemsByPregunta(element.pre_id)
                        .subscribe(items => {
                          this.items = items;
                          this.items.forEach( itm => {
                              if(element.pre_id === itm.pre_id) {
                                element.pre_sub_pregunta = this.items.slice();
                              }
                              if (itm.itm_combo !== 0) {
                                this.itemsComboSevice.getItemsCombo(itm.itm_combo)
                                .subscribe(itemsCombo => {
                                  this.itemsCombo = itemsCombo;
                                  if(itm.itm_combo !== 0) {
                                    itm.itm_combobox = this.itemsCombo.slice();
                                  }
                                });
                              }
                          });
                        });
                      });
              });
        });

    this.intrumentService.getInstrumentByCode(this.codigo)
        .subscribe(instrument => {
          this.instrument = instrument;
        });
        this.spinnerOff = 0;
        this.indiceView = 0;
  }

  convertJsonSubPregunt(subPregunt: string) {
    //console.log("Valor ->: ", subPregunt);
    this.subPreguntas = new Array();
    if (subPregunt[0] === '[' || subPregunt[0] === '{') {
      let listJsonString: string[] = subPregunt.split("}");
      let i: number = 0;
      let n:number = listJsonString.length;
      listJsonString.forEach(element => {
        let dataJson: ItemModel;
        if(i < (n - 1)) {
          if (i === 0) {
            element = element.substr(1);
          }

          let aux = JSON.parse(this.TransformJsonValid(element + '}'));
          this.subPreguntas.push(aux);
        }
        i ++;
      });
    }
    //console.log("Mirad ->: ", this.subPreguntas);
    return this.subPreguntas;
  }

  TransformJsonValid(valueJson : string) {
    let outJsonValid : any;
    let j : number;
    //if ((valueJson[0] !== ' ') && (valueJson[1] !== ',') && (valueJson[2] !== ' ')) {
    if (valueJson[0] === '{') {
      outJsonValid = valueJson[0];
      j = 1;
    } else {
      outJsonValid =  valueJson[2];
      j = 3;
    }

    //console.log("a reparar ->: ", valueJson);
    for (let i:number =  j; i < valueJson.length; i++) {
      if (valueJson[i] === "'") {
        outJsonValid = outJsonValid + '"';
      } else {
        if (valueJson[i] ) {
          outJsonValid = outJsonValid + valueJson[i];
        }
      }
    }
    return outJsonValid;
  }

  validationArrayPreguntasCheck(arrayPreguntasChecks) {
    let arrayOut: any[];
    arrayOut = new Array();
    for (let i:number = 0; i < (arrayPreguntasChecks.length - 1); i++)  {
      arrayOut.push(arrayPreguntasChecks[i]);
    }
    return arrayOut;
  }

  getCodigoAndUsername() {
    this.isUsername = false;
    this.codigo = "";
    this.username = "";
    for (let i = 0; i < this.value.length; i++) {
      if (this.value[i] === '_') {
        this.isUsername = true;
      } else {
        if (!this.isUsername) {
          this.codigo = this.codigo + (this.value[i]);
        } else {
          this.username = this.username + (this.value[i]);
        }
      }
    }
  }

  onSearchChange(searchValue) {
    console.log(searchValue);
  }

  goBack(country: string): void {
    //this.navactor.back();
  }

  accept(): void {
    //this.navactor.back();
  }

  detailEvaluationCheck() {
    this.router.navigate(['/dashboard/postgrado/actor/evaluations-check']);
  }

  nextData() {
    this.numberIndex = this.numberIndex + 1;
    this.arrayOut = this.arrayPreguntasChecks[this.numberIndex];
  }

  previousData() {
    this.numberIndex = this.numberIndex - 1;
    this.arrayOut = this.arrayPreguntasChecks[this.numberIndex];
  }
}
