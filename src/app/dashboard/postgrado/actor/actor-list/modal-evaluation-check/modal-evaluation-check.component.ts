import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

import 'rxjs/add/operator/switchMap';
import { ComboModel, ItemsComboModel } from '../../../../common-dashboard/models/evaluation.model';
import { ComboService } from '../../../../common-dashboard/services/combo.service';
import { ItemsComboService } from '../../../../common-dashboard/services/itemsCombo.service';

@Component({
  selector: 'app-modal-evaluation-check',
  templateUrl: './modal-evaluation-check.component.html',
  styleUrls: ['./modal-evaluation-check.component.css']
})
export class ModalEvaluationCheckComponent implements OnInit {
  data: string;
  combos: ComboModel[] = [];
  itemsCombo: ItemsComboModel[] = [];
  comboSaved: ComboModel;
  itemsComboSaved: ItemsComboModel;

  constructor(
    public dialogRef: MatDialogRef<ModalEvaluationCheckComponent>,
    private comboService: ComboService,
    private itemsComboService: ItemsComboService,
    public dialog: MatDialog,
    private router: Router
  ) { }

  ngOnInit() {
    this.comboService.getCombos()
        .subscribe(combos => {
          this.combos = combos;
        });
  }

  onChange(value) {
    this.itemsComboService.getItemsCombo(value.cmb_id)
        .subscribe(itemsCombo => {
          this.itemsCombo = itemsCombo;
        });
  }

  goBack() {
    this.dialogRef.close();
  }

  viewListEvaluationCheck() {
    this.dialogRef.close();
    this.router.navigate(['/dashboard/postgrado/actor/evaluations-check']);
  }
}
