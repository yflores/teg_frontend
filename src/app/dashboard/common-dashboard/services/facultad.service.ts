import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment.prod';
import { FacultadModel } from '../models/facultad.model';

@Injectable()
export class FacultadService {
    apiurl: string = environment.apiUrl;
    
    _urlNgnix = 'http://localhost:5000/facultad';
    _urlNgnixDetail = 'http://localhost:5000/combo';

    constructor(private http: Http) {}

    getFacultades(): Observable<FacultadModel[]> {
        return this.http.get(`${this._urlNgnix}/findallfacultad`)
        .map(response => response.json() as FacultadModel[]);
    }

    getFacultadById(id: number): Observable<FacultadModel> {
        return this.http.get(`${this._urlNgnix}/findonebyid/${id}`)
        .map(response => response.json() as FacultadModel);
    }

    postSaveUpdateFacultad(facultad: FacultadModel): Observable<FacultadModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/saveupdatefacultad`, JSON.stringify(facultad), {headers: headers})
                   .map(response => response.json());
    }

    postdeleteFacultad(facultad: FacultadModel): Observable<FacultadModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/deletefacultad`, JSON.stringify(facultad), {headers: headers})
                   .map(response => response.json());
    }
}
