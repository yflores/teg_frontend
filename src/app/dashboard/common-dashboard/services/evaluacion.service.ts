import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { EvaluacionModel } from '../models/evaluation.model';

@Injectable()
export class EvaluacionService {
    _urlNgnix = 'http://localhost:5000/evaluacions';
    _urlDetail = 'http://localhost:5000/evaluacion';

    constructor(private http: Http) {}

    getEvaluacions(): Observable<EvaluacionModel[]> {
      return this.http.get(this._urlNgnix)
                 .map(response => response.json() as EvaluacionModel[]);
    }

    getEvaluacionByCodeInstrument(code_instument: string): Observable<EvaluacionModel> {
        return this.http.get(`${this._urlDetail}/${code_instument}`)
                   .map(response => response.json() as EvaluacionModel);
      }

    getPreguntasByEvaluacion(code_instument: string): Observable<EvaluacionModel> {
        return this.http.get(`${this._urlDetail}/${code_instument}`)
                   .map(response => response.json() as EvaluacionModel);
      }

    postInstrument(evaluacion: EvaluacionModel): Observable<EvaluacionModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}`, JSON.stringify(evaluacion), {headers: headers})
        .map(response => response.json());
    }

    getListEvaluationsCheck(): Observable<any> {
      return this.http.get(`${this._urlNgnix}check`)
                 .map(response => response.json() as any[]);
    }

    getEvaluationsCheckByUsername(username: string): Observable<any> {
      return this.http.get(`${this._urlNgnix}checkcode/${username}`)
                 .map(response => response.json() as any);
    }
}
