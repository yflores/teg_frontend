import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProgramService } from '../../../common-dashboard/services/program.service';
import { ProgramModel } from '../../../common-dashboard/models/program.model';
import { FacultadService } from '../../../common-dashboard/services/facultad.service';
import { FacultadModel } from '../../../common-dashboard/models/facultad.model';
import { ModalProgramComponent } from './modal-program/modal-program.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'detail-faculty-program',
  templateUrl: './detail-faculty-program.component.html',
  styleUrls: ['./detail-faculty-program.component.css']
})
export class DetailFacultyProgramComponent implements OnInit {

  programsByFacultad : ProgramModel[] = [];
  program: ProgramModel;
  idFacultad: string;
  facultad: FacultadModel;
  spinner = true;

  constructor(
    private programService: ProgramService,
    private facultadService: FacultadService,
    private route: ActivatedRoute,
    public dialog: MatDialog
    ) { }

  ngOnInit() {
    this.program = new ProgramModel(0, "", 0, "");
    this.route.paramMap.switchMap((params: ParamMap) => this.idFacultad = params.get('id_facultad'))
    .subscribe(idFacultad => {
        this.idFacultad = idFacultad;
        this.programService.findAllprogramByFacultadService(idFacultad).subscribe(programsByFacultad => {
          this.programsByFacultad = programsByFacultad;
        });

        this.facultadService.getFacultadById(parseInt(this.idFacultad)).subscribe(facultad => {
          this.facultad = facultad;
        });
        this.spinner = false;
    });
  }

  openModalNewProgram() {
    const dialogRef = this.dialog.open(ModalProgramComponent, {
      height: '350px',
      width: '500px',
      data: {
        title: "Nuevo Programa",
        program: this.program,
        idFacultad: this.idFacultad
      }
    });
    dialogRef.beforeClose().subscribe(program => {
     this.ngOnInit();
    });
  }

  openModalEditProgram(program: ProgramModel) {
    const dialogRef = this.dialog.open(ModalProgramComponent, {
      height: '350px',
      width: '500px',
      data: {
        title: "Editar Programa",
        program: program,
        idFacultad: this.idFacultad
      }
    });
    dialogRef.beforeClose().subscribe(idCombo => {
     this.ngOnInit();
    });
  }

  deleteProgram(program: ProgramModel) {
    console.log("Programa ->: ", program);
    this.programService.postdeleteProgram(program).subscribe(program => {
      console.log("Programa Eliminado...");
      this.ngOnInit();
    });
  }
}
