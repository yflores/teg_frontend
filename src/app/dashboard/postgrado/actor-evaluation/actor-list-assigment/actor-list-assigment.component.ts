import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap, Params } from '@angular/router';

import 'rxjs/add/operator/switchMap';

import { MatDialog, MatSnackBar } from '@angular/material';

import { InstrumentService } from '../../../common-dashboard/services/instrument.service';
import { DataPersonalActorModel, InstrumentoModel } from '../../../common-dashboard/models/instrumento.models';

@Component({
  selector: 'app-actor-list-assigment',
  templateUrl: './actor-list-assigment.component.html',
  styleUrls: ['./actor-list-assigment.component.css']
})
export class ActorListAssigmentComponent implements OnInit {
  spinnerOff = true;
  dataPersonal: DataPersonalActorModel[] = [];
  instruments: InstrumentoModel[] = [];
  instrument: InstrumentoModel;

  matDialog: any;

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private instrumentService: InstrumentService
  ) { }

  ngOnInit() {
    this.instruments = new Array();
    this.route.paramMap
        .switchMap((params: ParamMap) => this.instrumentService.getInstrumentAssigmentByUser(params.get('username')))
        .subscribe(dataPersonal => {
          this.dataPersonal = dataPersonal;
          this.dataPersonal.forEach(element => {
            this.instrumentService.getInstrumentByCode(element.code_instrument)
                .subscribe(instrument => {
                  this.instrument = instrument;
                  this.instruments.push(this.instrument);
                  console.log("Salida -->: ", this.instruments);
                });
          });
          this.spinnerOff = false;
        });
  }
}
