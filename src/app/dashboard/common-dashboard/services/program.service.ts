import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { environment } from '../../../../environments/environment.prod';
import { ProgramModel } from '../models/program.model';

@Injectable()
export class ProgramService {
    apiurl: string = environment.apiUrl;
    
    _urlNgnix = 'http://localhost:5000/program';
    _urlNgnixDetail = 'http://localhost:5000/combo';

    constructor(private http: Http) {}

    findAllprogramByFacultadService(id_facultad: string): Observable<ProgramModel[]> {
        return this.http.get(`${this._urlNgnix}/programsbyfacultadid/${id_facultad}`) 
        .map(response => response.json() as ProgramModel[]);
    }

    postSaveUpdateProgram(program: ProgramModel): Observable<ProgramModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/saveupdateprogram`, JSON.stringify(program), {headers: headers})
                   .map(response => response.json());
    }

    postdeleteProgram(program: ProgramModel): Observable<ProgramModel> {
        let headers = new Headers();
        return this.http.post(`${this._urlNgnix}/deleteprogram`, JSON.stringify(program), {headers: headers})
                   .map(response => response.json());
    }

    getProgramById(id: number): Observable<ProgramModel> {
        return this.http.get(`${this._urlNgnix}/findonebyid/${id}`)
        .map(response => response.json() as ProgramModel);
    }
}
