import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';

import 'rxjs/add/operator/switchMap';

import { EvaluacionModel, PreguntaModel, ItemModel, ItemsComboModel } from '../../../common-dashboard/models/evaluation.model';
import { InstrumentoModel, dataInstrumentCheckModel } from '../../../common-dashboard/models/instrumento.models';
import { InstrumentService } from '../../../common-dashboard/services/instrument.service';
import { EvaluacionService } from '../../../common-dashboard/services/evaluacion.service';
import { PreguntaService } from '../../../common-dashboard/services/pregunta.service';
import { ItemService } from '../../../common-dashboard/services/item.service';
import { ItemsComboService } from '../../../common-dashboard/services/itemsCombo.service';
import { LoginService } from '../../../common-dashboard/services/login.service';
import { ProgramService } from '../../../common-dashboard/services/program.service';
import { ProgramModel } from '../../../common-dashboard/models/program.model';


@Component({
  selector: 'app-actor-resolution-evaluation',
  templateUrl: './actor-resolution-evaluation.component.html',
  styleUrls: ['./actor-resolution-evaluation.component.css']
})
export class ActorResolutionEvaluationComponent implements OnInit {

  instrument: InstrumentoModel;
  panelOpenState = false;
  codigo: string;
  evaluacion: EvaluacionModel;
  preguntas: PreguntaModel[] = [];
  items: ItemModel[] = [];
  spinnerOff = 1;
  itemsCombo: ItemsComboModel[] = [];

  checked = false;
  indeterminate = false;
  labelPosition = 'after';
  disabled = false;
  value: any;
  matriz: number;
  addNewDataMatriz: any[];
  username: string;
  dataIntCheck: dataInstrumentCheckModel;
  dialogRef: any;
  facultId: number;
  listProgramsByFacultyId: ProgramModel[] = [];

  constructor(
    public dialog: MatDialog,
    public nackBar: MatSnackBar,
    private route: ActivatedRoute,
    private intrumentService: InstrumentService,
    private evaluacionService: EvaluacionService,
    private preguntaService: PreguntaService,
    private itemService: ItemService,
    private itemsComboSevice: ItemsComboService,
    private router: Router,
    private loginService: LoginService,
    private programService: ProgramService
  ) { }

  ngOnInit() {
    this.addNewDataMatriz = new Array();

    this.route.paramMap
    .switchMap((params: ParamMap) => this.username = params.get('username'))
    .subscribe(username => {
        this.username.concat(username);
        console.log("Username ->: ", this.username);
    });

    this.loginService.getFacultyByUsername(this.username)
    .subscribe(facultyId => {
      this.facultId = facultyId;
      console.log("Facultad ->: ", this.facultId);
      this.programService.findAllprogramByFacultadService(this.facultId.toString())
      .subscribe(listProgramsByFacultyId => {
        this.listProgramsByFacultyId = listProgramsByFacultyId;
        console.log("Lista de programas de la facultad ->: ", this.listProgramsByFacultyId);
      });
    });

    this.route.paramMap
    .switchMap((params: ParamMap) => this.codigo = params.get('int_codigo'))
    .subscribe(codigo => {
        this.codigo.concat(codigo);
    });

    this.getPreguntsEvaluations();

    this.intrumentService.getInstrumentByCode(this.codigo)
        .subscribe(instrument => {
          this.instrument = instrument;
          this.matriz = this.instrument.int_lista_matriz;
          console.log("Matriz ->: ", this.instrument.int_lista_matriz);
        });
        this.spinnerOff = 0;
  }

  onSearchChange(searchValue: any) {
    console.log(searchValue);
  }

  goBack(country: string): void {
    //this.navactor.back();
  }

  sendEvaluation(preguntas: any, instrument: any) {
    console.log("Valor Original ->: ", preguntas);
    if (this.matriz === 0) {
      preguntas.forEach(element => {
        if (element.pre_sub_pregunta != undefined) {
          element.pre_sub_pregunta.forEach(subElement => {
            subElement.itm_combobox = "";
          });
        }
      });
    }
    console.log("Preguntas Resueltas ->: ", preguntas);
    this.addNewDataMatriz.push(preguntas);
    this.preguntaService.postPreguntasCheck(this.addNewDataMatriz, this.username)
      .subscribe(element => {
        console.log("Preguntas guardadas");
    });
    this.dataIntCheck = new dataInstrumentCheckModel(undefined, undefined, undefined);
    console.log("Mira el valor -->: ", this.dataIntCheck);
    this.dataIntCheck.codigo = instrument;
    this.dataIntCheck.username = this.username;
    //this.dataIntCheck.prod_id = parseInt(this.value);
    this.intrumentService.posteDataEvaluationCheckForActor(this.dataIntCheck)
    .subscribe(data => {
      console.log("Actualizacion de Estatus de Evaluacion Lista.");
    });
    this.router.navigate(['/dashboard/postgrado/actor-evaluation/' + this.username]);
  }

  addNewData(preguntas: PreguntaModel) {
    console.log("preguntas ->: ", preguntas);
    this.addNewDataMatriz.push(preguntas);
    console.log("Columnas de la Matriz ->: ", this.addNewDataMatriz);
    this.getPreguntsEvaluations();
  }

  getPreguntsEvaluations() {
    this.evaluacionService.getEvaluacionByCodeInstrument(this.codigo)
        .subscribe(evaluacion => {
          this.evaluacion = evaluacion;
          this.preguntaService.getPreguntasByEvaluacion(this.evaluacion.eval_id)
              .subscribe(preguntas => {
                this.preguntas = preguntas;

                  this.preguntas.forEach(element => {
                    this.itemService.getItemsByPregunta(element.pre_id)
                        .subscribe(items => {
                          this.items = items;
                          this.items.forEach( itm => {
                              if(element.pre_id === itm.pre_id) {
                                element.pre_sub_pregunta = this.items.slice();
                              }
                              if(itm.itm_combo > 0) {
                                this.itemsComboSevice.getItemsCombo(itm.itm_combo)
                                .subscribe(itemsCombo => {
                                  this.itemsCombo = itemsCombo;
                                  if(itm.itm_combo !== 0) {
                                    itm.itm_combobox = this.itemsCombo.slice();
                                  }
                                });
                              }
                          });
                        });
                      });
              });
        });
  }
}
