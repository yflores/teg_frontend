
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { InstrumentoModel, GeneradorModel, DataPersonalActorModel } from '../../../../common-dashboard/models/instrumento.models';
import { InstrumentService } from '../../../../common-dashboard/services/instrument.service';
import { Information } from '../../../../../information';
import { UserService } from './../../../../common-dashboard/services/user.service';
import { LoginModel } from '../../../../common-dashboard/models/login.models';

@Component({
  selector: 'app-generation-aplication',
  templateUrl: './generation-aplication.component.html',
  styleUrls: ['./generation-aplication.component.css']
})
export class GenerationAplicationComponent implements OnInit {
  dialogRef: any;
  instrumentsBack: any [];
  instrumentPrograma: InstrumentoModel;
  instrument: InstrumentoModel;
  instruments: InstrumentoModel[];
  dataCantidades: GeneradorModel;
  numeroDecimal: number;
  numeroEspecifico: number;
  dataAsignadaProfesor: DataPersonalActorModel[];
  dataAsignadaParticipante: DataPersonalActorModel[];
  dataAsignadaEgresado: DataPersonalActorModel[];
  dataAsignadaEmpleado: DataPersonalActorModel[];
  viewActive: number;
  bandera: boolean;
  username: string = Information.getCookieByName('username');
  user: LoginModel;

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private instrumentoService: InstrumentService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.viewActive = 0;
    this.bandera = false;
    this.dataAsignadaProfesor = new Array();
    this.dataAsignadaParticipante = new Array();
    this.dataAsignadaEgresado = new Array();
    this.dataAsignadaEmpleado = new Array();
    this.instrumentPrograma = new InstrumentoModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
    this.instruments = new Array();

    this.userService.getUserByUsername(this.username)
    .subscribe(user => {
      this.user = user;

      this.instrumentoService.getInstrumentsByFacultyId(this.user.fac_id.toString())
      .subscribe(instruments => {
        this.instrumentsBack = instruments;
        this.instrumentsBack.forEach(element => {
          this.instrument = new InstrumentoModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
          this.instrument.int_id = element.id;
          this.instrument.int_codigo = element.codigo;
          this.instrument.int_iniciales = element.iniciales;
          this.instrument.int_nombre = element.nombre;
          this.instrument.int_descripcion = element.descripcion;
          this.instrument.int_lista_matriz = element.lista_matriz;
          this.instrument.int_programa = element.programa;
          this.instrument.int_profesor = element.profesor;
          this.instrument.int_participante = element.participante;
          this.instrument.int_egresado = element.egresado;
          this.instrument.int_empleado = element.empleado;
          this.dataCantidades = new GeneradorModel(undefined, undefined, new Array(), undefined, undefined, new Array(), undefined, undefined, new Array(), undefined, undefined, new Array());
          this.instrument.asignacion = this.dataCantidades;
          this.instruments.push(this.instrument);
        });
      });
    })
  }

  onChangePrograma(programa) {
    console.log("Programa ->: ", programa);
  }

  onChange(searchValue) {
    if (searchValue.cantidad_profesores !== undefined) {
      searchValue.calculo_profesores = searchValue.cantidad_profesores;
      if (searchValue.cantidad_profesores > 5) {
        searchValue.calculo_profesores = (searchValue.calculo_profesores * 0.30);
        searchValue.calculo_profesores = this.redodeoNumber(searchValue.calculo_profesores);
      }
      searchValue.array_cant_profesores = this.createArray(searchValue.calculo_profesores);
      this.dataAsignadaProfesor = this.createDataAssignamentByNumber(searchValue.calculo_profesores, this.dataAsignadaProfesor);
    } else {
      searchValue.calculo_profesores = null;
    }

    if (searchValue.cantidad_participante !== undefined) {
      searchValue.calculo_participantes = searchValue.cantidad_participante;
      if (searchValue.cantidad_participante > 5) {
        searchValue.calculo_participantes = (searchValue.calculo_participantes * 0.30);
        searchValue.calculo_participantes = this.redodeoNumber(searchValue.calculo_participantes);
      }
      searchValue.array_cant_participantes = this.createArray(searchValue.calculo_participantes);
      this.dataAsignadaParticipante = this.createDataAssignamentByNumber(searchValue.calculo_participantes, this.dataAsignadaParticipante);
    } else {
      searchValue.calculo_participantes = null;
    }

    if (searchValue.cantidad_egresado !== undefined) {
      searchValue.calculo_egresados = searchValue.cantidad_egresado;
      if (searchValue.cantidad_egresado > 5) {
        searchValue.calculo_egresados = (searchValue.calculo_egresados * 0.30);
        searchValue.calculo_egresados = this.redodeoNumber(searchValue.calculo_egresados);
      }
      searchValue.array_cant_egresados = this.createArray(searchValue.calculo_egresados);
      this.dataAsignadaEgresado = this.createDataAssignamentByNumber(searchValue.calculo_egresados, this.dataAsignadaEgresado);
    } else {
      searchValue.calculo_egresados = null;
    }

    if (searchValue.cantidad_empleado !== undefined) {
      searchValue.calculo_empleados = searchValue.cantidad_empleado;
      if (searchValue.cantidad_empleado > 5) {
        searchValue.calculo_empleados = (searchValue.calculo_empleados * 0.30);
        searchValue.calculo_empleados = this.redodeoNumber(searchValue.calculo_empleados);
      }
      searchValue.array_cant_empleados = this.createArray(searchValue.calculo_empleados);
      this.dataAsignadaEmpleado = this.createDataAssignamentByNumber(searchValue.calculo_empleados, this.dataAsignadaEmpleado);
    } else {
      searchValue.calculo_empleados = null;
    }

    let banderaAux = false;
    this.instruments.forEach(element => {
    console.log("oye vale ->: ", element.asignacion);
      if (element.asignacion.array_cant_egresados.length > 0 || element.asignacion.array_cant_empleados.length > 0 ||
        element.asignacion.array_cant_participantes.length > 0 || element.asignacion.array_cant_profesores.length > 0) {
        this.bandera = true;
        banderaAux = true;
      }
      if (element.asignacion.array_cant_egresados.length === 0 && element.asignacion.array_cant_empleados.length === 0 &&
        element.asignacion.array_cant_participantes.length === 0 && element.asignacion.array_cant_profesores.length === 0 &&
        !banderaAux) {
        this.bandera = false;
      }
    });
  }

  createArray(cantidad) {
    let array = [];
    for (let i = 0; i < cantidad; i++) {
      array.push(i + 1);
    }
    return array;
  }

  goBack() {
    this.dialogRef.close();
  }

  redodeoNumber(cantidad: number) {
    console.log("Cantidad sin Redondear --->: ", cantidad);
    this.numeroEspecifico = 0;
    this.numeroEspecifico = this.numberInitDecimal(cantidad.toString());
    if (this.numeroEspecifico >= 5) {
      this.numeroEspecifico = parseInt(this.numberInteger(cantidad.toString())) + 1;
    } else {
      this.numeroEspecifico = parseInt(this.numberInteger(cantidad.toString()));
    }
    console.log("Cantidad Redondeada --->: ", this.numeroEspecifico);
    return this.numeroEspecifico;
  }

  numberInitDecimal(numberString: string) {
    this.numeroDecimal = 0;
    for (let index = 0; index < numberString.length; index++) {
      const elementArray = numberString[index];
      if (elementArray === ".") {
        this.numeroDecimal = parseInt(numberString[index + 1]);
        break;
      }
    }
    return this.numeroDecimal;
  }

  numberInteger(numberString: string) {
    let valueEnd;
    for (let index = 0; index < numberString.length; index++) {
      const elementArray = numberString[index];
      if (elementArray === ".") {
        valueEnd = index;
        break;
      }
    }
    return numberString.substring(0, valueEnd);
  }

  sendAndSaveEvaluation(instrument_code, actor, dataAssignament, indice) {
    dataAssignament.actor = actor;
    dataAssignament.code_instrument = instrument_code;
    dataAssignament.faculty_id = Information.getCookieByName("facultyId");
    console.log("Data Save ->: ", dataAssignament);
    this.instrumentoService.postSendAndSaveDataEvaluationForActor(dataAssignament)
        .subscribe(element => {
          console.log("Save Data DataBase...");
          dataAssignament.marca = 1;
          switch (actor) {
            case "Profesor":
              this.deleteElementArrayEnvios(this.dataAsignadaProfesor, indice);
              break;

            case "Participante":
              this.deleteElementArrayEnvios(this.dataAsignadaParticipante, indice);
              break;

            case "Egresado":
              this.deleteElementArrayEnvios(this.dataAsignadaEgresado, indice);
              break;

            case "Empleado":
              this.deleteElementArrayEnvios(this.dataAsignadaEmpleado, indice);
              break;
          }
        });
  }

  deleteElementArrayEnvios(dataAssignament, indice) {
    dataAssignament.splice(indice, 1)
    ;
  }

  onChangeAssignament(dataAssignament) {
    console.log("dataAssignament --->: ", dataAssignament);
  }

  createDataAssignamentByNumber(cant: number, dataAsignada) {
    dataAsignada = this.createArray(cant);
    for (let i = 0; i < cant; i++) {
      dataAsignada[i] = new DataPersonalActorModel(undefined, undefined, undefined, undefined, undefined, undefined, undefined, 0, 0);
    }
    return dataAsignada;
  }

  viewActiveEvaluationActor() {
    this.viewActive = 1;
    this.bandera = false;
  }
}
