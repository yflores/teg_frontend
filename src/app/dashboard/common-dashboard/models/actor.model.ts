export class ActorModel {
    constructor(
        public act_id: number,
        public act_nombre: string,
        public act_edad: number,
        public act_cedula: string,
        public act_fechanac: Date,
        public act_usu: number
    ) { }
}
