import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComboPreguntaSeleccionMultipleComponent } from './combo-pregunta-seleccion-multiple.component';

describe('ComboPreguntaSeleccionMultipleComponent', () => {
  let component: ComboPreguntaSeleccionMultipleComponent;
  let fixture: ComponentFixture<ComboPreguntaSeleccionMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComboPreguntaSeleccionMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComboPreguntaSeleccionMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
