import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import { MatDialog, MatSnackBar } from '@angular/material';
import { InstrumentoModel } from '../../../../common-dashboard/models/instrumento.models';
import { InstrumentService } from '../../../../common-dashboard/services/instrument.service';
import { EvaluacionService } from '../../../../common-dashboard/services/evaluacion.service';
import { ModalEvaluationCheckComponent } from '../modal-evaluation-check/modal-evaluation-check.component';

@Component({
  selector: 'app-list-evaluation-check',
  templateUrl: './list-evaluation-check.component.html',
  styleUrls: ['./list-evaluation-check.component.css']
})
export class ListEvaluationCheckComponent implements OnInit {
  spinnerOff = true;
  matDialog: any;
  evalsCheck: any[] = [];
  arrayEvalByUser: string[];

  constructor(
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private evaluationService: EvaluacionService
  ) { }

  ngOnInit() {
    this.arrayEvalByUser = new Array();
    this.evaluationService.getListEvaluationsCheck()
    .subscribe(evalsCheck => {
      this.evalsCheck = evalsCheck;
      console.log("EvalsCheck ->: ", this.evalsCheck);
      this.spinnerOff = false;
    });
  }

  openNewEvaluation() {
    this.router.navigate(['/dashboard/postgrado/actor/list-evaluation-check/new-instrumento']);
  }

  openGenerationEvaluationActors() {
    this.router.navigate(['/dashboard/postgrado/actor/list-evaluation-check/generation-aplication']);
  }
}
