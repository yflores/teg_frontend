import { NgModule } from '@angular/core';
import { MatDialogModule, MatFormFieldModule, MatInputModule } from '@angular/material';

import { routing } from './actor-evaluation.routing';
import { CommonDashboardModule } from '../../common-dashboard/common-dashboard.module';
import { DashboardMaterialModule } from '../../common-dashboard/dashboard-material/dashboard-material.module';
import { PostgradoMaterialModule } from '../actor/postgrado-material/postgrado-material.module';

import { InstrumentService } from "../../common-dashboard/services/instrument.service";
import { EvaluacionService } from '../../common-dashboard/services/evaluacion.service';
import { PreguntaService } from '../../common-dashboard/services/pregunta.service';
import { ItemService } from '../../common-dashboard/services/item.service';
import { ItemsComboService } from '../../common-dashboard/services/itemsCombo.service';
import { ComboService } from '../../common-dashboard/services/combo.service';
import { ActorListAssigmentComponent } from './actor-list-assigment/actor-list-assigment.component';
import { ActorResolutionEvaluationComponent } from './actor-resolution-evaluation/actor-resolution-evaluation.component';

@NgModule({
    imports: [
        CommonDashboardModule,
        DashboardMaterialModule,
        PostgradoMaterialModule,
        routing,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
    ],
    declarations: [
        ActorListAssigmentComponent,
        ActorResolutionEvaluationComponent
    ],
    providers: [
        InstrumentService,
        EvaluacionService,
        PreguntaService,
        ItemService,
        ItemsComboService,
        ComboService,
    ],
    entryComponents: [
        ActorListAssigmentComponent,
        ActorResolutionEvaluationComponent

    ]
})
export class ActorEvaluationModule {}
