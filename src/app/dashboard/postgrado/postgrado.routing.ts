import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostgradoComponent } from './postgrado.component';

const routes: Routes = [
  {
    path: '',
    component: PostgradoComponent,
    children: [
      {
        path: 'actor',
        loadChildren: './actor/actor.module#ActorModule'
      },
      {
        path: 'actor-evaluation',
        loadChildren: './actor-evaluation/actor-evaluation.module#ActorEvaluationModule'
      },
      {
        path: 'administrator',
        loadChildren: './admin/admin.module#AdminModule'
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
