export class DataPersonalActorModel {
  constructor(
      public actor: string,
      public code_instrument: string,
      public primer_nombre: string,
      public segundo_apellido: string,
      public identification: string,
      public email: string,
      public active: number,
      public marca: number,
      public facultad: number
  ) {}
}

export class GeneradorModel {
  constructor(
      public cantidad_profesores: number,
      public calculo_profesores: number,
      public array_cant_profesores: number[],
      public cantidad_participante: number,
      public calculo_participantes: number,
      public array_cant_participantes: number[],
      public cantidad_egresado: number,
      public calculo_egresados: number,
      public array_cant_egresados: number[],
      public cantidad_empleado: number,
      public calculo_empleados: number,
      public array_cant_empleados: number[]
  ) { }
}

export class InstrumentoModel {
  constructor(
      public int_id: number,
      public int_codigo: string,
      public int_iniciales: string,
      public int_nombre: string,
      public int_descripcion: string,
      public int_lista_matriz: number,
      public int_programa: string,
      public int_profesor: number,
      public int_participante: number,
      public int_egresado: number,
      public int_empleado: number,
      public asignacion: GeneradorModel,
      public fac_id: number
  ) { }

  public setypePerson(typePerson: String, value: number) {
    console.log("eje si sirve");
    if (typePerson === "int_profesor") {
      this.int_profesor = value;
    } else {
      if (typePerson === "int_participante") {
        this.int_participante = value;
      } else {
        if (typePerson === "int_egresado") {
          this.int_egresado = value;
        } else {
          if (typePerson === "int_empleado") {
            this.int_empleado = value;
          }
        }
      }
    }
  }
}

export class dataInstrumentCheckModel {
  constructor(
      public username: string,
      public codigo: string,
      public prod_id: number
  ) { }
}
