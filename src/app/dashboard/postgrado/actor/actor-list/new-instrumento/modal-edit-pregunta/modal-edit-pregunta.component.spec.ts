import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEditPreguntaComponent } from './modal-edit-pregunta.component';

describe('ModalEditPreguntaComponent', () => {
  let component: ModalEditPreguntaComponent;
  let fixture: ComponentFixture<ModalEditPreguntaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEditPreguntaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEditPreguntaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
