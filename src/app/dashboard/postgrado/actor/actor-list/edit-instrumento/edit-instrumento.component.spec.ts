import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInstrumentoComponent } from './edit-instrumento.component';

describe('EditInstrumentoComponent', () => {
  let component: EditInstrumentoComponent;
  let fixture: ComponentFixture<EditInstrumentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInstrumentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInstrumentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
